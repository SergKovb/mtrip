//
//  MTRoutesDataDisplayManagerTests.m
//  MTripTests
//

#import <XCTest/XCTest.h>
#import "MTRoutesDataDisplayManager.h"

@interface MTRoutesDataDisplayManagerTests : XCTestCase

@property (nonatomic, strong) MTRoutesDataDisplayManager *dataDisplayManager;

@end

@implementation MTRoutesDataDisplayManagerTests

- (void)setUp {
    [super setUp];
    
    self.dataDisplayManager = [MTRoutesDataDisplayManager new];
}

- (void)tearDown {
    self.dataDisplayManager = nil;
    
    [super tearDown];
}

- (void)testThatDataDisplayManagerReturnsTableViewDataSource {
    // given
    
    // when
    id <UITableViewDataSource> dataSource = [self.dataDisplayManager dataSourceForTableView:nil];
    
    // then
    XCTAssertNotNil(dataSource);
}

@end
