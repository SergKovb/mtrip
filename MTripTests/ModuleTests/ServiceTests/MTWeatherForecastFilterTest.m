//
//  MTWeatherForecastFilterTest.m
//  MTripTests
//

#import <XCTest/XCTest.h>
#import "MTWeatherForecastFilterImplementation.h"
#import "MTripTestsConstants.h"
#import "MTWeatherForecastModel.h"
#import "MTWeatherModel.h"

@interface MTWeatherForecastFilterTest : XCTestCase

@property (nonatomic, strong) MTWeatherForecastFilterImplementation *weatherForecastFilter;

@end

@implementation MTWeatherForecastFilterTest

- (void)setUp {
    [super setUp];
    
    self.weatherForecastFilter = [MTWeatherForecastFilterImplementation new];
}

- (void)tearDown {
    self.weatherForecastFilter = nil;
    
    [super tearDown];
}

- (void)testFilterWeather {
    // given
    let forecastModel = [self generateWeatherForecastModelForTestPurposes];
    let date = [NSDate dateWithTimeIntervalSince1970:1551085200];
    
    // when
    let weatherModel = [self.weatherForecastFilter weatherModelFromForecast:forecastModel
                                                            forTimeInterval:800
                                                                  sinceDate:date];
    
    // then
    XCTAssertNotNil(weatherModel);

}

- (MTWeatherForecastModel *)generateWeatherForecastModelForTestPurposes {
    let bundleName = @"MTModelObject";
    let modelName = NSStringFromClass([MTWeatherForecastModel class]);
    let fileName = [NSString stringWithFormat:@"%@.json", modelName];
    
    let resourceBundle = [NSBundle bundleForClass:[self class]];
    let pathToTestBundle = [resourceBundle pathForResource:bundleName ofType:@"bundle"];
    let testBundle = [NSBundle bundleWithPath:pathToTestBundle];
    let pathToFile = [[testBundle resourcePath] stringByAppendingPathComponent:fileName];
    NSString *responseString = [NSString stringWithContentsOfFile:pathToFile
                                                         encoding:NSUTF8StringEncoding error:nil];
    let weatherForecastModel = [[MTWeatherForecastModel alloc] initWithString:responseString error:nil];
    
    return weatherForecastModel;
}

@end
