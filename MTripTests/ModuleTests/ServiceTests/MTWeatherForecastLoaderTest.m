//
//  MTWeatherForecastLoaderTest.m
//  MTripTests
//

#import <XCTest/XCTest.h>
#import <MapKit/MapKit.h>
#import "MTWeatherForecastLoaderImplementation.h"
#import "MTripTestsConstants.h"
#import "MTWeatherForecastModel.h"

@interface MTWeatherForecastLoaderTest : XCTestCase

@property (nonatomic, strong) MTWeatherForecastLoaderImplementation *weatherForecastLoader;

@end

@implementation MTWeatherForecastLoaderTest

- (void)setUp {
    [super setUp];
    
    self.weatherForecastLoader = [MTWeatherForecastLoaderImplementation new];
}

- (void)tearDown {
    self.weatherForecastLoader = nil;
    
    [super tearDown];
}

- (void)testLoadWeather {
    // given
    let expectation = [self expectationWithDescription:@"weather forecast load"];
    let location = [[CLLocation alloc] initWithLatitude:55.794775
                                              longitude:37.580359];
    
    __block MTWeatherForecastModel *resultWeatherModel;
    
    //when
    [self.weatherForecastLoader loadWeatherForLocation:location
                                                 block:^(MTWeatherForecastModel *weatherModel) {
                                                     resultWeatherModel = weatherModel;
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         [expectation fulfill];
                                                     });
                                                 }];
    
    // then
    [self waitForExpectationsWithTimeout:kTestExpectationTimeout
                                 handler:^(NSError * _Nullable error) {
                                     XCTAssertNotNil(resultWeatherModel);
                                     XCTAssertLessThan(resultWeatherModel.lat - location.coordinate.latitude, 0.005);
                                 }];
}

- (void)testLoadFailure {
    // given
    let expectation = [self expectationWithDescription:@"weather forecast fail"];
    let location = [[CLLocation alloc] initWithLatitude:91
                                              longitude:37.580359];

    __block BOOL failure;
    __block NSError *resultError;
    __block MTWeatherForecastModel *resultWeatherModel;

    //when
    [self.weatherForecastLoader loadWeatherForLocation:location
                                                 block:^(MTWeatherForecastModel *weatherModel) {
                                                     resultWeatherModel = weatherModel;
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         [expectation fulfill];
                                                     });
                                                 } failure:^(NSError *error) {
                                                     failure = YES;
                                                     resultError = error;
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         [expectation fulfill];
                                                     });
                                                 }];

    // then
    [self waitForExpectationsWithTimeout:kTestExpectationTimeout
                                 handler:^(NSError * _Nullable error) {
                                     XCTAssertNil(resultWeatherModel);
                                     XCTAssertEqual(failure, YES);
                                     XCTAssertEqual(resultError.code, -1011);
                                 }];
}

@end
