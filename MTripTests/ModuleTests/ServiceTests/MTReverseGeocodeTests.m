//
//  MTReverseGeocodeTests.m
//  MTripTests
//

#import "MTripTestsConstants.h"
#import <XCTest/XCTest.h>
#import <MapKit/MapKit.h>
#import "MTGeocodeItem.h"
#import "MTReverseGeocodeImplementation.h"

@interface MTReverseGeocodeTests : XCTestCase

@property (nonatomic, strong) MTReverseGeocodeImplementation *reverseGeocode;

@end

@implementation MTReverseGeocodeTests

- (void)setUp {
    [super setUp];
    
    self.reverseGeocode = [MTReverseGeocodeImplementation new];
}

- (void)tearDown {
    self.reverseGeocode = nil;
    
    [super tearDown];
}

- (void)testLoadAppleReverseGeocode {
    // given
    let expectation = [self expectationWithDescription:@"placemark obtain"];
    let location = [[CLLocation alloc] initWithLatitude:55.794775
                                              longitude:37.580359];
    __block CLPlacemark *resultPlacemark;

    //when
    [self.reverseGeocode loadAppleReverseGeocodeWithLocation:location block:^(CLPlacemark *placemark) {
        resultPlacemark = placemark;
        dispatch_async(dispatch_get_main_queue(), ^{
            [expectation fulfill];
        });
    } failure:nil];
    

    // then
    [self waitForExpectationsWithTimeout:kTestExpectationTimeout
                                 handler:^(NSError * _Nullable error) {
                                     XCTAssertNotNil(resultPlacemark);
                                     XCTAssertEqualObjects([resultPlacemark name], @"улица 2-я Квесисская, 13");
                                     XCTAssertEqualObjects([resultPlacemark country], @"Россия");
                                 }];
}


- (void)testLoadYandexReverseGeocode {
    // given
    let expectation = [self expectationWithDescription:@"geoObject obtain"];
    let location = [[CLLocation alloc] initWithLatitude:55.794775
                                              longitude:37.580359];
    __block MTGeoObject *resultGeoObject;
    
    //when
    [self.reverseGeocode loadYandexReverseGeocodeWithLocation:location
                                                        block:^(MTGeoObject *geoObject) {
                                                            resultGeoObject = geoObject;
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                [expectation fulfill];
                                                            });
                                                        } failure:nil];
    
    // then
    [self waitForExpectationsWithTimeout:kTestExpectationTimeout
                                 handler:^(NSError * _Nullable error) {
                                     XCTAssertNotNil(resultGeoObject);
                                     XCTAssertEqualObjects(resultGeoObject.name, @"2-я Квесисская улица, 13");
                                     XCTAssertEqualObjects(resultGeoObject.countryName, @"Россия");
                                 }];
}

@end
