//
//  MTDirectionsFinderTests.m
//  MTripTests
//


#import <XCTest/XCTest.h>
#import <MapKit/MapKit.h>
#import "MTRouteFinderImplementation.h"
#import "MTripTestsConstants.h"

@interface MTRouteFinderTests : XCTestCase

@property (nonatomic, strong) MTRouteFinderImplementation *routeFinder;

@end

@implementation MTRouteFinderTests

- (void)setUp {
    [super setUp];
    
    self.routeFinder = [MTRouteFinderImplementation new];
}

- (void)tearDown {
    self.routeFinder = nil;
    
    [super tearDown];
}

- (void)testRouteFind {
    // given
    let expectation = [self expectationWithDescription:@"route find"];
    let location = [[CLLocation alloc] initWithLatitude:55.794775
                                              longitude:37.580359];
    
    __block MKRoute *resultRoute;
    
    //when
    [self.routeFinder routeFrom:location
                             to:location
                        success:^(MKRoute *route) {
                            resultRoute = route;
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [expectation fulfill];
                            });
                        }];
    
    // then
    [self waitForExpectationsWithTimeout:kTestExpectationTimeout
                                 handler:^(NSError * _Nullable error) {
                                     XCTAssertNotNil(resultRoute);
                                     XCTAssertEqual(resultRoute.distance, 0);
                                 }];
}

- (void)testRouteFailure {
    // given
    let expectation = [self expectationWithDescription:@"route failure"];
    let startLocation = [[CLLocation alloc] initWithLatitude:55.794775
                                              longitude:37.580359];
    let finishLocation = [[CLLocation alloc] initWithLatitude:0
                                                    longitude:0];
    __block BOOL failure;
    __block NSError *resultError;

    //when
    [self.routeFinder routeFrom:startLocation
                             to:finishLocation
                        success:^(MKRoute *route) {
                        } failure:^(NSError *error) {
                            failure = YES;
                            resultError = error;
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [expectation fulfill];
                            });
                        }];
    
    // then
    [self waitForExpectationsWithTimeout:kTestExpectationTimeout
                                 handler:^(NSError * _Nullable error) {
                                     XCTAssertEqual(failure, YES);
                                     XCTAssertEqual(resultError.code, 2);
                                 }];
}

@end
