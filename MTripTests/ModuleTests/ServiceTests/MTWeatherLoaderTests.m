//
//  MTWeatherLoaderTests.m
//  MTripTests
//

#import <XCTest/XCTest.h>
#import <MapKit/MapKit.h>
#import "MTWeatherLoaderImplementation.h"
#import "MTripTestsConstants.h"
#import "MTWeatherModel.h"

@interface MTWeatherLoaderTests : XCTestCase

@property (nonatomic, strong) MTWeatherLoaderImplementation *weatherLoader;

@end

@implementation MTWeatherLoaderTests

- (void)setUp {
    [super setUp];
    
    self.weatherLoader = [MTWeatherLoaderImplementation new];
}

- (void)tearDown {
    self.weatherLoader = nil;
    
    [super tearDown];
}

- (void)testLoadWeather {
    // given
    let expectation = [self expectationWithDescription:@"weather load"];
    let location = [[CLLocation alloc] initWithLatitude:55.794775
                                              longitude:37.580359];
    
    __block MTWeatherModel *resultWeatherModel;
    
    //when
    [self.weatherLoader loadCurrentWeatherForLocation:location
                                                block:^(MTWeatherModel *weatherModel) {
                                                    resultWeatherModel = weatherModel;
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        [expectation fulfill];
                                                    });
                                                }];
    // then
    [self waitForExpectationsWithTimeout:kTestExpectationTimeout
                                 handler:^(NSError * _Nullable error) {
                                     XCTAssertNotNil(resultWeatherModel);
                                     XCTAssertLessThan(resultWeatherModel.tempMax, 50);
                                     XCTAssertGreaterThan(resultWeatherModel.tempMin, -50);
                                 }];
}

- (void)testLoadFailure {
    // given
    let expectation = [self expectationWithDescription:@"weather fail"];
    let location = [[CLLocation alloc] initWithLatitude:91
                                              longitude:37.580359];

    __block BOOL failure;
    __block NSError *resultError;
    __block MTWeatherModel *resultWeatherModel;

    //when
    [self.weatherLoader loadCurrentWeatherForLocation:location
                                                block:^(MTWeatherModel *weatherModel) {
                                                    resultWeatherModel = weatherModel;
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        [expectation fulfill];
                                                    });
                                                }failure:^(NSError *error) {
                                                    failure = YES;
                                                    resultError = error;
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        [expectation fulfill];
                                                    });
                                                }];
    
    // then
    [self waitForExpectationsWithTimeout:kTestExpectationTimeout
                                 handler:^(NSError * _Nullable error) {
                                     XCTAssertNil(resultWeatherModel);
                                     XCTAssertEqual(failure, YES);
                                     XCTAssertEqual(resultError.code, -1011);
                                 }];
}
@end
