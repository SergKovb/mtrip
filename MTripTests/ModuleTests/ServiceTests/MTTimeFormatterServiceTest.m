//
//  MTTimeFormatterServiceTest.m
//  MTripTests
//

#import <XCTest/XCTest.h>
#import "MTTimeFormatterImplementation.h"
#import "MTWeatherForecastModel.h"
#import "MTWeatherModel.h"

@interface MTTimeFormatterServiceTest : XCTestCase

@property (nonatomic, strong) MTTimeFormatterImplementation *timeFormatter;

@end

@implementation MTTimeFormatterServiceTest

- (void)setUp {
    [super setUp];
    
    self.timeFormatter = [MTTimeFormatterImplementation new];
}

- (void)tearDown {
    self.timeFormatter = nil;
    
    [super tearDown];
}


- (void)testFormatTime {
    // given
    let timeInterval = 150000;
    
    // when
    let result = [self.timeFormatter stringFromTravelTimeInterval:timeInterval];
    
    // then
    XCTAssertTrue([result isEqualToString:@" 41 ч: 40 мин"]);

}
@end
