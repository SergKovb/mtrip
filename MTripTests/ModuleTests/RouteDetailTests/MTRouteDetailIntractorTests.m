//
//  MTRouteDetailIntractorTests.m
//  MTripTests
//

#import <XCTest/XCTest.h>
#import <OCMock.h>
#import <CoreLocation/CoreLocation.h>

#import "MTRouteDetailInteractor.h"
#import "MTRouteItem.h"
#import "MTWeatherLoaderService.h"
#import "MTRouteFinderService.h"
#import "MTReverseGeocodeService.h"


@interface MTRouteDetailInteractorTests : XCTestCase

@property (nonatomic, strong) MTRouteDetailInteractor *interactor;
@property (nonatomic, strong) id <MTWeatherLoaderService> mockWeatherLoaderService;
@property (nonatomic, strong) id <MTRouteFinderService> mockRouteFinderService;
@property (nonatomic, strong) id <MTReverseGeocodeService> mockReverseGeocodeService;

@property (nonatomic, strong) id mockPonsomizer;
@property (nonatomic, strong) id mockLectureService;

@end

@implementation MTRouteDetailInteractorTests


- (void)setUp {
    [super setUp];
    
    self.interactor = [MTRouteDetailInteractor new];
    self.mockWeatherLoaderService = OCMProtocolMock(@protocol(MTWeatherLoaderService));
    self.mockRouteFinderService = OCMProtocolMock(@protocol(MTRouteFinderService));
    self.mockReverseGeocodeService = OCMProtocolMock(@protocol(MTReverseGeocodeService));

    self.interactor.weatherLoaderService = self.mockWeatherLoaderService;
    self.interactor.routeFinderService = self.mockRouteFinderService;
    self.interactor.reverseGeocodeService = self.mockReverseGeocodeService;
}

- (void)tearDown {

    [super tearDown];
}

//- (void)obtainRouteDetailsForRouteItem:(MTRouteItem *)routeItem;

- (void)testThatInteractorLoadWeather {
    // given
    let routeItem = [MTRouteItem routeItemWithName:@"test" lat:0 lon:0];
    
    // when
    [self.interactor obtainRouteDetailsForRouteItem:routeItem];
    
    // then
    OCMVerify([self.mockWeatherLoaderService loadCurrentWeatherForLocation:routeItem.destination
                                                                     block:OCMOCK_ANY]);
}

- (void)testThatInteractorObtainDirection {
    // given
    let routeItem = [MTRouteItem routeItemWithName:@"test" lat:0 lon:0];

    // when
    [self.interactor obtainRouteDetailsForRouteItem:routeItem];
    
    // then
    OCMVerify([self.mockRouteFinderService routeFrom:OCMOCK_ANY
                                                  to:routeItem.destination
                                             success:OCMOCK_ANY]);
}

- (void)testThatInteractorLoadGeocode {
    // given
    let routeItem = [MTRouteItem routeItemWithName:@"test" lat:0 lon:0];
    
    // when
    [self.interactor obtainRouteDetailsForRouteItem:routeItem];
    
    // then
    OCMVerify([self.mockReverseGeocodeService loadAppleReverseGeocodeWithLocation:routeItem.destination
                                                                            block:OCMOCK_ANY
                                                                          failure:OCMOCK_ANY]);
}

@end
