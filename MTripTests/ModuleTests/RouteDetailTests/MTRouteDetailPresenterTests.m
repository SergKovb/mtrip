//
//  RouteDetailPresenterTests.m
//  MTripTests
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import <MapKit/MapKit.h>

#import "MTRouteItem.h"
#import "MTWeatherModel.h"
#import "MTRouteDetailPresenter.h"
#import "MTRouteDetailInteractorIO.h"
#import "MTRouteDetailViewInput.h"
#import "MTRouteDetailViewInput.h"
#import "MTRouteDetailStepsData.h"



@interface MTRouteDetailPresenterTests : XCTestCase

@property (nonatomic, strong) MTRouteDetailPresenter *presenter;
@property (nonatomic, strong) id <MTRouteDetailInteractorInput> interactorMock;
@property (nonatomic, strong) id <MTRouteDetailViewInput> viewMock;
@property (nonatomic, strong) id routerMock;

@end

@implementation MTRouteDetailPresenterTests

- (void)setUp {
    [super setUp];
    
    self.presenter = [MTRouteDetailPresenter new];
    self.interactorMock = OCMProtocolMock(@protocol(MTRouteDetailInteractorInput));
    self.viewMock = OCMProtocolMock(@protocol(MTRouteDetailViewInput));
    self.routerMock = OCMProtocolMock(@protocol(MTRouteDetailRouterInput));

    self.presenter.routeDetailInteractor = self.interactorMock;
    self.presenter.userInterface = self.viewMock;
    self.presenter.routeDetailRouter = self.routerMock;
}

- (void)tearDown {
    self.presenter = nil;
    [(id)self.interactorMock stopMocking];
    self.interactorMock = nil;
    [(id)self.viewMock stopMocking];
    self.viewMock = nil;
    [self.routerMock stopMocking];
    self.routerMock = nil;
    
    [super tearDown];
}

#pragma mark MTRouteDetailInput


- (void)testSuccessConfigureForRouteItem {
    // given
    let routeItem = [MTRouteItem routeItemWithName:@"testRoute" lat:0 lon:0];
    // when
    [self.presenter configureForRouteItem:routeItem];
    
    // then
    OCMVerify([self.interactorMock obtainRouteDetailsForRouteItem:routeItem]);
}

#pragma mark MTRouteDetailInteractorOutput

- (void)testSuccessDidLoadRouteDetailStepsData  {
    // given
    let routeDetailStepsData = [MTRouteDetailStepsData new];
    
    // when
    [self.presenter.userInterface showRouteDetailStepsData:routeDetailStepsData];
    
    // then
    OCMVerify([self.viewMock showRouteDetailStepsData:routeDetailStepsData]);
}

- (void)testSuccessDidLoadDestinationNameAndState  {
    // given
    NSString *name = @"testName";
    NSString *state = @"testState";
    
    // when
    [self.presenter.userInterface showDestinationName:name
                                                state:state];
    
    // then
    OCMVerify([self.viewMock showDestinationName:name
                                           state:state]);
}

- (void)testSuccessDidLoadWeather  {
    // given
    MTWeatherModel *weather = [MTWeatherModel new];
    
    // when
    [self.presenter.userInterface showCurrentWeather:weather];
    
    // then
    OCMVerify([self.viewMock showCurrentWeather:weather]);
}

@end
