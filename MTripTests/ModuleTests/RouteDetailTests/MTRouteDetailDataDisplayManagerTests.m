//
//  MTRouteDetailDataDisplayManagerTests.m
//  MTripTests
//


#import <XCTest/XCTest.h>
#import "MTRouteDetailDataDisplayManager.h"

@interface MTRouteDetailDataDisplayManagerTests : XCTestCase

@property (nonatomic, strong) MTRouteDetailDataDisplayManager *dataDisplayManager;

@end

@implementation MTRouteDetailDataDisplayManagerTests

- (void)setUp {
    [super setUp];
    
    self.dataDisplayManager = [MTRouteDetailDataDisplayManager new];
}

- (void)tearDown {
    self.dataDisplayManager = nil;
    
    [super tearDown];
}

- (void)testThatDataDisplayManagerReturnsTableViewDataSource {
    // given
    
    // when
    id <UITableViewDataSource> dataSource = [self.dataDisplayManager dataSourceForTableView:nil];
    
    // then
    XCTAssertNotNil(dataSource);
}


@end
