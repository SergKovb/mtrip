//
//  MTripTestsConstants.h
//  MTrip
//

#ifndef MTripTestsConstants_h
#define MTripTestsConstants_h

static NSTimeInterval const kTestExpectationTimeout = 2.f;

#endif /* MTripTestsConstants_h */
