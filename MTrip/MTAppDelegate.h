//
//  AppDelegate.h
//  MTrip
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface MTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

