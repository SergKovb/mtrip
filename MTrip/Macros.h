// MIT License
//
// Copyright (c) 2018 Joom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#import <Foundation/Foundation.h>

#define let __auto_type const

#define var __auto_type

#define JM_FINAL_CLASS __attribute__((objc_subclassing_restricted))

#define JM_BOXABLE __attribute__((objc_boxable))

#define JM_OVERLOADABLE __attribute__((overloadable))

#define JM_WARN_UNUSED_RESULT __attribute__((warn_unused_result))

NS_ASSUME_NONNULL_BEGIN

@interface NSArray<__covariant ObjectType> (ForeachSupport)

@property (nonatomic, strong, readonly) ObjectType jm_enumeratedType;

@end

@interface NSSet<__covariant ObjectType> (ForeachSupport)

@property (nonatomic, strong, readonly) ObjectType jm_enumeratedType;

@end

@interface NSDictionary<__covariant KeyType, __covariant ObjectType> (ForeachSupport)

@property (nonatomic, strong, readonly) KeyType jm_enumeratedType;

@end

@interface NSOrderedSet<__covariant ObjectType> (ForeachSupport)

@property (nonatomic, strong, readonly) ObjectType jm_enumeratedType;

@end

@interface NSPointerArray (ForeachSupport)

@property (nonatomic, assign, readonly) void *jm_enumeratedType;

@end

@interface NSHashTable<ObjectType> (ForeachSupport)

@property (nonatomic, strong, readonly) ObjectType jm_enumeratedType;

@end

@interface NSMapTable<KeyType, ObjectType> (ForeachSupport)

@property (nonatomic, strong, readonly) KeyType jm_enumeratedType;

@end

@interface NSEnumerator<ObjectType> (ForeachSupport)

@property (nonatomic, strong, readonly) ObjectType jm_enumeratedType;

@end

NS_ASSUME_NONNULL_END

#define foreach(object_, collection_) for (typeof((collection_).jm_enumeratedType) object_ in (collection_))

#define JMNonnull(obj_) \
    ({ \
        NSCAssert(object_, @"Expected `%@` not to be nil.", @#obj_); \
        (typeof({ __auto_type result_ = (object_); result_; }))(object_); \
    })
