//
//  AppDelegate.m
//  MTrip
//


#import "MTAppDelegate.h"

@interface MTAppDelegate ()

@end

@implementation MTAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window.tintColor = [UIColor whiteColor];
    return YES;
}

@end
