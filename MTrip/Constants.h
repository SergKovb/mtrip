//
//  Constants.h
//  MTrip
//

static let APPID = @"3294fbb1429b6c71a7769075be79bd42"; //openWeather indetifier

static let YandexReverseGeocodeURL = @"https://geocode-maps.yandex.ru/1.x/?geocode=%f,%f&format=json";
static let WeatherUrl = "http://api.openweathermap.org/data/2.5/weather";
static let WeatherForeCastUrl = "http://api.openweathermap.org/data/2.5/forecast";
static let WeatherUrlImg = "http://openweathermap.org/img/w/";
