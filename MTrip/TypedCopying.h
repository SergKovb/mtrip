// MIT License
//
// Copyright (c) 2018 Joom
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#import "Macros.h"

NS_ASSUME_NONNULL_BEGIN

@interface NSArray<__covariant ObjectType> (TypedCopying)

- (NSArray<ObjectType> *)copy JM_WARN_UNUSED_RESULT;

- (NSMutableArray<ObjectType> *)mutableCopy JM_WARN_UNUSED_RESULT;

@end

@interface NSSet<__covariant ObjectType> (TypedCopying)

- (NSSet<ObjectType> *)copy JM_WARN_UNUSED_RESULT;

- (NSMutableSet<ObjectType> *)mutableCopy JM_WARN_UNUSED_RESULT;

@end

@interface NSDictionary<__covariant KeyType, __covariant ObjectType> (TypedCopying)

- (NSDictionary<KeyType, ObjectType> *)copy JM_WARN_UNUSED_RESULT;

- (NSMutableDictionary<KeyType, ObjectType> *)mutableCopy JM_WARN_UNUSED_RESULT;

@end

@interface NSOrderedSet<__covariant ObjectType> (TypedCopying)

- (NSOrderedSet<ObjectType> *)copy JM_WARN_UNUSED_RESULT;

- (NSMutableOrderedSet<ObjectType> *)mutableCopy JM_WARN_UNUSED_RESULT;

@end

@interface NSPointerArray (TypedCopying)

- (NSPointerArray *)copy JM_WARN_UNUSED_RESULT;

@end

@interface NSHashTable<ObjectType> (TypedCopying)

- (NSHashTable<ObjectType> *)copy JM_WARN_UNUSED_RESULT;

@end

@interface NSMapTable<KeyType, ObjectType> (TypedCopying)

- (NSMapTable<KeyType, ObjectType> *)copy JM_WARN_UNUSED_RESULT;

@end

@interface NSString (TypedCopying)

- (NSString *)copy JM_WARN_UNUSED_RESULT;

- (NSMutableString *)mutableCopy JM_WARN_UNUSED_RESULT;

@end

@interface NSAttributedString (TypedCopying)

- (NSAttributedString *)copy JM_WARN_UNUSED_RESULT;

- (NSMutableAttributedString *)mutableCopy JM_WARN_UNUSED_RESULT;

@end

@interface NSIndexSet (TypedCopying)

- (NSIndexSet *)copy JM_WARN_UNUSED_RESULT;

- (NSMutableIndexSet *)mutableCopy JM_WARN_UNUSED_RESULT;

@end

NS_ASSUME_NONNULL_END
