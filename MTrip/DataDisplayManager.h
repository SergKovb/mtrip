//
//  DataDisplayManager.h
//  MTrip
//

@protocol DataDisplayManager <NSObject>

- (id<UITableViewDataSource>)dataSourceForTableView:(UITableView *)tableView;
- (id<UITableViewDelegate>)delegateForTableView:(UITableView *)tableView;

@end
