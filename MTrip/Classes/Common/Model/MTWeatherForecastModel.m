//
//  MTWeatherForecastModel.m
//  MTrip
//

#import "MTWeatherForecastModel.h"

@implementation MTWeatherForecastModel

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"lat": @"city.coord.lat",
                                                                  @"lon": @"city.coord.lon",
                                                                  @"weatherModels": @"list",
                                                                  }];
}
@end
