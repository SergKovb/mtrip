//
//  MTRouteStep.h
//  MTrip
//


#import <Foundation/Foundation.h>

@class MKRoute;
@class MTManagedRouteStep;

NS_ASSUME_NONNULL_BEGIN

@interface MTRouteStep : NSObject

@property (nonatomic) NSInteger         travelTime;
@property (nonatomic) float             lat;
@property (nonatomic) float             lon;
@property (nonatomic) float             distance;

+ (instancetype)routeStepWithRoute:(MKRoute *)route;
+ (instancetype)routeStepWithManagedRouteStep:(MTManagedRouteStep *)managedRouteSteps;

@end

NS_ASSUME_NONNULL_END
