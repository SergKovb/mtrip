//
//  MTRouteModel.m
//  MTrip
//


#import "MTRouteItem.h"
#import "MTRouteStep.h"
#import <CoreLocation/CoreLocation.h>

@interface MTRouteItem ()

@property (nonatomic) CLLocation *location;

@end

@implementation MTRouteItem

+ (instancetype)routeItemWithName:(NSString *)name
                              lat:(float)lat
                              lon:(float)lon {
    let routeItem = [MTRouteItem new];
    
    routeItem.name = name;
    routeItem.lat = lat;
    routeItem.lon = lon;
    
    routeItem.location = [[CLLocation alloc] initWithLatitude:lat
                                                    longitude:lon];

    return routeItem;
}

- (void)setRouteSteps:(NSArray<MTRouteStep *> *)routeSteps {
    _routeSteps = routeSteps;
}

- (void)setManagedRouteSteps:(NSOrderedSet <MTManagedRouteStep *> *)managedRouteSteps {
    let routeSteps = (NSMutableArray <MTRouteStep *> *)[NSMutableArray new];
    foreach(managedRouteStep, managedRouteSteps) {
        let routeStep = [MTRouteStep routeStepWithManagedRouteStep:managedRouteStep];
        [routeSteps addObject:routeStep];
    }
    self.routeSteps = [routeSteps copy];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"MTRouteItem name:%@, lat:%f, lon:%f route steps: %@",_name,_lat,_lon,_routeSteps];
}

- (CLLocation *)destination {
    return self.location;
}

@end
