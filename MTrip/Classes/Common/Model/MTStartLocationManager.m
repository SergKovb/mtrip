//
//  MTStartLocation.m
//  MTrip
//

#import "MTStartLocationManager.h"


@implementation MTStartLocationManager

- (instancetype)init {
    if ((self = [super init])) {
        self.startLocation = [self getSavedLocation];
    }
    return self;
}

- (CLLocation *)getSavedLocation {
    let locationData = (NSData *)[[NSUserDefaults standardUserDefaults] objectForKey:@"SavedLocation"];
    if (locationData) {
        let myStoredLocation = (CLLocation *)[NSKeyedUnarchiver unarchiveObjectWithData:locationData];
        return myStoredLocation;
    } else {
        return NULL;
    }
}

- (void)setStartLocation:(CLLocation *)location {
    _startLocation = location;
    let locationData = [NSKeyedArchiver archivedDataWithRootObject:location];
    [[NSUserDefaults standardUserDefaults] setObject:locationData forKey:@"SavedLocation"];
}

@end
