//
//  MTStartLocation.h
//  MTrip
//

#import <Foundation/Foundation.h>

@class CLLocation;

@interface MTStartLocationManager : NSObject

@property (nonatomic) CLLocation *startLocation;

- (void)setStartLocation:(CLLocation *)location;

@end
