//
//  MTWeatherForecastModel.h
//  MTrip
//

#import "JSONModel.h"
#import "MTWeatherModel.h"

@protocol MTWeatherModel;

NS_ASSUME_NONNULL_BEGIN

@interface MTWeatherForecastModel : JSONModel

@property float             lat;
@property float             lon;
@property (nonatomic) NSArray <MTWeatherModel *> <MTWeatherModel> *weatherModels;

@end

NS_ASSUME_NONNULL_END
