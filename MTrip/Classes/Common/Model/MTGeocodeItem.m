//
//  MTGeocodeItem.m
//  MTrip
//

#import "MTGeocodeItem.h"


@implementation MTGeocodeItem

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"featureMember": @"response.GeoObjectCollection.featureMember"
                                                                  }];
}

@end


@implementation MTGeoObject
+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"desc": @"GeoObject.description",
                                                                  @"name": @"GeoObject.name",
                                                                  @"countryName":@"GeoObject.metaDataProperty.GeocoderMetaData.AddressDetails.Country.CountryName",
                                                                  @"administrativeAreaName": @"GeoObject.metaDataProperty.GeocoderMetaData.AddressDetails.Country.AdministrativeArea.AdministrativeAreaName",
                                                                  @"addressLine": @"GeoObject.metaDataProperty.GeocoderMetaData.AddressDetails.Country.AddressLine",
                                                                  @"point": @"GeoObject.Point.pos",
                                                                  @"lat": @"GeoObject.Point.pos",
                                                                  @"lon": @"GeoObject.Point.pos",
                                                                  }];
}

- (BOOL)validate:(NSError **)error {
    if (![super validate:error])
        return NO;
    
    let regex = [NSRegularExpression
                 regularExpressionWithPattern:@"([\\d]+\\.[\\d]{3,})"
                 options:0
                 error:error];
    
    
    
    if ([regex matchesInString:self.point options:0 range:NSMakeRange(0, [self.point length])].count != 2 && error!=NULL) {
        let userInfoError = [NSDictionary dictionaryWithObject:@"lat & lon validation" forKey:NSLocalizedDescriptionKey];
        *error = [NSError errorWithDomain:@"geocode-maps.yandex.ru" code:1 userInfo:userInfoError];
        
        return NO;
    }
    
    return YES;
}

- (void)setLonWithNSString:(NSString *)string {
    NSError *error  = NULL;
    let regex = [NSRegularExpression
                 regularExpressionWithPattern:@"([\\d]+\\.[\\d]{3,})"
                 options:0
                 error:&error];
    let range  = [regex rangeOfFirstMatchInString:string
                                          options:0
                                            range:NSMakeRange(0, [string length])];
    let result = [string substringWithRange:range];
    self.lon = [result floatValue];
}

- (void)setLatWithNSString:(NSString *)string{
    NSError  *error  = NULL;
    let regex = [NSRegularExpression
                 regularExpressionWithPattern:@"([\\d]+\\.[\\d]{3,})"
                 options:0
                 error:&error];
    let result = [string substringWithRange:[regex matchesInString:self.point options:0 range:NSMakeRange(0, [self.point length])].lastObject.range];
    self.lat = [result floatValue];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"MTGeoObject name: %@ \n addres: %@",_name,_addressLine];
}
@end



