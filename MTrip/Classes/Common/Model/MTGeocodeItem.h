//
//  MTGeocodeItem.h
//  MTrip
//

#import <Foundation/Foundation.h>
#import <JSONModel/JSONModel.h>

@protocol MTGeoObject;


@interface MTGeoObject : JSONModel
@property (nonatomic) NSString    <Optional>      *desc;
@property (nonatomic) NSString          *name;
@property (nonatomic) NSString          *point;
@property (nonatomic) NSString    <Optional>       *countryName;
@property (nonatomic) NSString    <Optional>       *administrativeAreaName;
@property (nonatomic) NSString    <Optional>       *addressLine;
@property (nonatomic) float          lat;
@property (nonatomic) float          lon;

@end

@interface MTGeocodeItem  : JSONModel

@property (nonatomic) NSArray <MTGeoObject> *featureMember;

@end
