//
//  MTWeatherModel.h
//  MTrip
//

#import <JSONModel/JSONModel.h>

@protocol MTWeatherItem;

@interface MTWeatherItem : JSONModel
@property (nonatomic) NSString          *desc;
@property (nonatomic) NSString          *main;
@property (nonatomic) NSString          *icon;
@property (nonatomic) NSInteger         id;

- (NSURL *)urlForIcon;

@end

@interface MTWindItem : JSONModel
@property float             deg;
@property float             speed;
@end

@interface MTWeatherModel : JSONModel
@property NSString  <Optional>        *cityName;
@property float             temp;
@property float             tempMax;
@property float             tempMin;
@property NSInteger         humidity;
@property NSInteger         clouds;
@property NSInteger         dt;
@property (nonatomic) NSArray <MTWeatherItem *> <MTWeatherItem> *weatherItems;
@property MTWindItem        *wind;

- (NSString *)temperatureString;
- (NSString *)temperatureRangeString;
- (NSString *)timeString;

@end


