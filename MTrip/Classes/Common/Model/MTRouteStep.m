//
//  MTRouteStep.m
//  MTrip
//

#import "MTRouteStep.h"
#import <MapKit/MKDirectionsResponse.h>
#import <MapKit/MKPolyline.h>
#import "MTManagedRouteStep.h"

@implementation MTRouteStep

+ (instancetype)routeStepWithRoute:(MKRoute *)route {
    let routeStep = [MTRouteStep new];
    
    routeStep.lat = route.polyline.coordinate.latitude;
    routeStep.lon = route.polyline.coordinate.longitude;
    routeStep.travelTime = route.expectedTravelTime;
    routeStep.distance = route.distance;

    return routeStep;
}

+ (instancetype)routeStepWithManagedRouteStep:(MTManagedRouteStep *)managedRouteStep {
    let routeStep = [MTRouteStep new];
    
    routeStep.lat = managedRouteStep.lat;
    routeStep.lon = managedRouteStep.lon;
    routeStep.distance = managedRouteStep.distance;
    routeStep.travelTime = managedRouteStep.travelTime;
    
    return  routeStep;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"MTRouteStep distance:%f, travelTime:%ld lat:%f, lon:%f", _distance, (long)_travelTime, _lat, _lon];
}

@end
