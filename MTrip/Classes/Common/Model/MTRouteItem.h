//
//  MTRouteModel.h
//  MTrip
//

#import <Foundation/Foundation.h>

@class MTRouteStep;
@class MTManagedRouteStep;
@class CLLocation;

@interface MTRouteItem : NSObject

@property (nonatomic, retain) NSString*         name;
@property (nonatomic) float             lat;
@property (nonatomic) float             lon;
@property (nonatomic) float             distance;
@property (nonatomic, retain) NSArray <MTRouteStep *>   *routeSteps;


+ (instancetype)routeItemWithName:(NSString *)name
                              lat:(float)lat
                              lon:(float)lon;

- (void)setRouteSteps:(NSArray<MTRouteStep *> *)routeSteps;
- (void)setManagedRouteSteps:(NSArray<MTManagedRouteStep *> *)routeSteps;
- (CLLocation *)destination;

@end
