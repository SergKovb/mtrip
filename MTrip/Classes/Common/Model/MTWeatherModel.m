//
//  MTWeatherModel.m
//  MTrip
//

#import "MTWeatherModel.h"
#import "Constants.h"


@implementation MTWeatherModel

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"weatherItems": @"weather",
                                                                  @"humidity": @"main.humidity",
                                                                  @"temp": @"main.temp",
                                                                  @"tempMax": @"main.temp_max",
                                                                  @"tempMin": @"main.temp_min",
                                                                  @"cityName": @"name",
                                                                  @"clouds": @"clouds.all"
                                                                  }];
}

- (NSString *)temperatureString {
    return  [NSString stringWithFormat:@"%2.1fº",self.temp];
}

- (NSString *)timeString {
    let dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"MM-dd HH:mm"];
    let date = [NSDate dateWithTimeIntervalSince1970:self.dt];
    return  [dateFormatter stringFromDate:date];
}


- (NSString *)temperatureRangeString {
    if (self.tempMax == self.tempMin) {
        return [NSString stringWithFormat:@"%2.1fº",self.tempMax];
    } else {
        return [NSString stringWithFormat:@"%2.1fº - %2.1fº",self.tempMin, self.tempMax];
    }
}

@end

@implementation MTWeatherItem

+ (JSONKeyMapper *)keyMapper {
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"desc": @"description",
                                                                  }];
}

- (NSURL *)urlForIcon {
    let imgURL = [NSString stringWithFormat:@"%s/%@.png",WeatherUrlImg, _icon];
    return  [NSURL URLWithString:imgURL];
}

@end
