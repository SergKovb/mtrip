//
//  MTRoutesDataStore.h
//  MTrip
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MTRouteItem;
@class MTManagedRouteItem;
@class MTManagedRouteStep;

typedef void(^MTDataStoreFetchCompletionBlock)(NSArray <MTManagedRouteItem *> *results);

@interface MTRoutesDataStore : NSObject

- (void)fetchRouteItemsWithCompletionBlock:(MTDataStoreFetchCompletionBlock)completionBlock;
- (MTManagedRouteItem *)newRouteItem;
- (MTManagedRouteStep *)newRouteStep;
- (void)deleteRouteWithLat:(float)lat
                       lon:(float)lon
                   success:(void (^)(void))success;
- (void)save;

@end
