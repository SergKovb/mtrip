//
//  MTManagedRouteStep.m
//  MTrip
//

#import "MTManagedRouteStep.h"

@implementation MTManagedRouteStep

@dynamic lat;
@dynamic lon;
@dynamic distance;
@dynamic travelTime;
@dynamic routeItem;

@end
