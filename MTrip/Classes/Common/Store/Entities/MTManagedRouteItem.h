//
//  MTManagedRouteItem.h
//  MTrip
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "MTManagedRouteStep.h"


@interface MTManagedRouteItem : NSManagedObject

@property (nonatomic, retain) NSString*         name;
@property (nonatomic) float                     lat;
@property (nonatomic) float                     lon;
@property (nonatomic) float                     distance;
@property (nonatomic, strong) NSOrderedSet <MTManagedRouteStep *> *routeSteps;

@end
