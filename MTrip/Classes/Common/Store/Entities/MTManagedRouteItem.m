//
//  MTManagedRouteItem.m
//  MTrip
//


#import "MTManagedRouteItem.h"

@implementation MTManagedRouteItem

@dynamic name;
@dynamic lat;
@dynamic lon;
@dynamic distance;
@dynamic routeSteps;

@end
