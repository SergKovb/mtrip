//
//  MTManagedRouteStep.h
//  MTrip
//

#import <CoreData/CoreData.h>

@class MTManagedRouteItem;

NS_ASSUME_NONNULL_BEGIN

@interface MTManagedRouteStep : NSManagedObject

@property (nonatomic) float                     lat;
@property (nonatomic) float                     lon;
@property (nonatomic) float                     distance;
@property (nonatomic) unsigned long             travelTime;
@property (nonatomic,retain) MTManagedRouteItem             *routeItem;

@end

NS_ASSUME_NONNULL_END
