//
//  MTRoutesDataStore.m
//  MTrip
//

#import "MTRoutesDataStore.h"
#import "MTManagedRouteItem.h"

@interface MTRoutesDataStore ()

@property (nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end


@implementation MTRoutesDataStore


- (id)init {
    if ((self = [super init])) {
        _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
        
        _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
        
        NSError *error = nil;
        let applicationDocumentsDirectory = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
        let options = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                                 [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
        let storeURL = [applicationDocumentsDirectory URLByAppendingPathComponent:@"MTrip-route.sqlite"];
        
        [_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                  configuration:nil
                                                            URL:storeURL
                                                        options:options error:&error];
        
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        _managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator;
        _managedObjectContext.undoManager = nil;

    }
    return self;
}

- (void)fetchRouteItemsWithCompletionBlock:(MTDataStoreFetchCompletionBlock)completionBlock {
    [self fetchRouteItemsWithPredicate:nil completionBlock:completionBlock];
}

- (void)fetchRouteItemsWithPredicate:(NSPredicate *)predicate
                  completionBlock:(MTDataStoreFetchCompletionBlock)completionBlock {
    let fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"RouteItem"];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:nil];
    [self.managedObjectContext performBlock:^{
        NSError *error = nil;
        let results = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        if (completionBlock)
        {
            completionBlock(results);
        }
    }];
}

- (void)deleteRouteWithLat:(float)lat
                       lon:(float)lon
                   success:(void (^)(void))success {
    let predicate = [NSPredicate predicateWithFormat:@"lat == %f AND lon == %f ", lat, lon];
    [self fetchRouteItemsWithPredicate:predicate
                       completionBlock:^(NSArray<MTManagedRouteItem *> *results) {
                           foreach(object, results) {
                               [self.managedObjectContext deleteObject:object];
                           }
                           NSError *error = nil;
                           if ([self.managedObjectContext save:&error] == NO) {
                               NSLog(@"error %@",error.description);
                           } else {
                               success();
                           }
                       }];
}


- (MTManagedRouteItem *)newRouteItem {
    let entityDescription = [NSEntityDescription entityForName:@"RouteItem"
                                                         inManagedObjectContext:self.managedObjectContext];
    let newEntry = (MTManagedRouteItem *)[[NSManagedObject alloc] initWithEntity:entityDescription
                                                                  insertIntoManagedObjectContext:self.managedObjectContext];
    return newEntry;
}

- (MTManagedRouteStep *)newRouteStep{
    let entityDescription = [NSEntityDescription entityForName:@"RouteStep"
                                        inManagedObjectContext:self.managedObjectContext];
    let newEntry = (MTManagedRouteStep *)[[NSManagedObject alloc] initWithEntity:entityDescription
                                                  insertIntoManagedObjectContext:self.managedObjectContext];
    return newEntry;
}

- (void)save {
    [self.managedObjectContext save:NULL];
}

@end

