//
//  MTRouteDetailModuleAssembly.m
//  MTrip
//


#import "MTRouteDetailModuleAssembly.h"
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "MTStartModuleAssembly.h"
#import "MTServiceComponentAssembly.h"

#import "MTRouteDetailViewController.h"
#import "MTRouteDetailInteractor.h"
#import "MTRouteDetailRouter.h"
#import "MTRouteDetailPresenter.h"
#import "MTStartLocationManager.h"
#import "MTRouteDetailDataDisplayManager.h"
#import "MTRoutesDataStore.h"

@interface MTRouteDetailModuleAssembly ()

@property (nonatomic,strong,readonly) MTStartModuleAssembly         *startModuleAssembly;
@property (nonatomic,strong,readonly) MTServiceComponentAssembly    *serviceComponentAssembly;

@end


@implementation MTRouteDetailModuleAssembly

- (id<RamblerViperModuleFactoryProtocol>)factoryRouteDetailModule {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardRouteDetailModule]];
                                                  [initializer injectParameterWith:@"MTRouteDetailViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardRouteDetailModule {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Main"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}

- (MTRouteDetailViewController *)viewModuleRouteDetail {
    return [TyphoonDefinition withClass:[MTRouteDetailViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterModuleRouteDetail]];
                              [definition injectProperty:@selector(dataDisplayManager)
                                                    with:[self dataDisplayManagerModuleRouteDetail]];
                          }];
}

- (MTRouteDetailPresenter *)presenterModuleRouteDetail {
    return [TyphoonDefinition withClass:[MTRouteDetailPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(userInterface)
                                                    with:[self viewModuleRouteDetail]];
                              [definition injectProperty:@selector(routeDetailInteractor)
                                                    with:[self interactorModuleRouteDetail]];
                              [definition injectProperty:@selector(routeDetailRouter)
                                                    with:[self routerModuleRouteDetail]];
                          }];
}

- (MTRouteDetailInteractor *)interactorModuleRouteDetail {
    return [TyphoonDefinition withClass:[MTRouteDetailInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStartLocationManager:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self.startModuleAssembly locationManagerModuleStart]];
                                              }];
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterModuleRouteDetail]];
                              [definition injectProperty:@selector(reverseGeocodeService)
                                                    with:[self.serviceComponentAssembly reverseGeocodeService]];
                              [definition injectProperty:@selector(routeFinderService)
                                                    with:[self.serviceComponentAssembly routeFinderService]];
                              [definition injectProperty:@selector(weatherLoaderService)
                                                    with:[self.serviceComponentAssembly weatherLoaderService]];
                              [definition injectProperty:@selector(weatherForecastLoaderService)
                                                    with:[self.serviceComponentAssembly weatherForecastLoaderService]];
                              [definition injectProperty:@selector(timeFormatterService)
                                                    with:[self.serviceComponentAssembly timeFormatterService]];
                              [definition injectProperty:@selector(weatherForecastFilterService)
                                                    with:[self.serviceComponentAssembly weatherForecastFilterService]];
                          }];
}

- (MTRouteDetailRouter *)routerModuleRouteDetail {
    return [TyphoonDefinition withClass:[MTRouteDetailRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                          }];
}

- (MTRouteDetailDataDisplayManager *)dataDisplayManagerModuleRouteDetail {
    return [TyphoonDefinition withClass:[MTRouteDetailDataDisplayManager class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(timeFormatter)
                                                    with:[self.serviceComponentAssembly timeFormatterService]];
                              [definition injectProperty:@selector(delegate)
                                                    with:[self presenterModuleRouteDetail]];
                          }];
}

@end
