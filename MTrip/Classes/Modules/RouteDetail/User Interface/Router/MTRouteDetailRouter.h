//
//  MTRouteDetailRouter.h
//  MTrip
//
#import <Foundation/Foundation.h>

@class MTRouteDetailPresenter;
@class MTRouteDetailViewController;
@class MTRootRouter;
@class MTRouteItem;

@interface MTRouteDetailRouter : NSObject

@property (nonatomic, strong) MTRouteDetailPresenter *routeDetailPresenter;

@end

