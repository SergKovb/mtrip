//
//  MTRouteDetailPresenter.h
//  MTrip
//
#import <Foundation/Foundation.h>

#import "MTRouteDetailViewOutput.h"
#import "MTRouteDetailInteractorIO.h"
#import "MTRouteDetailRouterInput.h"
#import "MTRouteDetailViewInput.h"
#import "MTRouteDetailInput.h"

@protocol MTRouteDetailViewInput;

@interface MTRouteDetailPresenter : NSObject <MTRouteDetailInteractorOutput, MTRouteDetailViewOutput, MTRouteDetailInput>

@property (nonatomic, strong) id<MTRouteDetailInteractorInput>          routeDetailInteractor;
@property (nonatomic, strong) id<MTRouteDetailRouterInput>              routeDetailRouter;
@property (nonatomic, weak) id<MTRouteDetailViewInput>                userInterface;

@end
