//
//  MTRouteDetailStepsData.m
//  MTrip
//

#import "MTRouteDetailStepsData.h"
#import "MTWeatherModel.h"



@implementation MTRouteDetailStepsData

+ (instancetype)upcomingDisplayDataWithRoutes:(NSArray <MTRouteSteps> *)routeSteps {
    let data = [[MTRouteDetailStepsData alloc] init];
    
    data.stepsObjects = routeSteps;
    
    return data;
}

- (BOOL)isEqualToUpcomingDisplayData:(MTRouteDetailStepsData *)data {
    if (!data) {
        return NO;
    }
    BOOL hasEqualRoutes = [self.stepsObjects isEqualToArray:data.stepsObjects];
    return hasEqualRoutes;
}

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    
    if (![object isKindOfClass:[MTRouteDetailStepsData class]]) {
        return NO;
    }
    
    return [self isEqualToUpcomingDisplayData:object];
}

- (NSUInteger)hash {
    return [self.stepsObjects hash];
}

@end

@implementation MTStepObject

+ (instancetype)routeStepWithDistance:(float)distance
                                 name:(NSString *)name
                                state:(NSString *)state
                           travelTime:(NSInteger)travelTime
                              weather:(MTWeatherModel *)weather {
    let routeStep = [MTStepObject new];
    routeStep.name = name;
    routeStep.distance = distance;
    routeStep.state = state;
    routeStep.weather = weather;
    routeStep.travelTime = travelTime;
    return routeStep;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%f name %@,temp %f",_distance,_name,_weather.temp];
}

@end
