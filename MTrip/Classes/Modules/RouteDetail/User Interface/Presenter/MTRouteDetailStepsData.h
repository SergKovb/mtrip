//
//  MTRouteDetailStepsData.h
//  MTrip
//

#import <Foundation/Foundation.h>
@protocol MTRouteSteps;
@class MTWeatherModel;


@interface MTStepObject : NSObject
@property (nonatomic) NSString          *name;
@property (nonatomic) NSString          *state;
@property (nonatomic) float             distance;
@property (nonatomic) NSInteger         travelTime;
@property (nonatomic) MTWeatherModel    *weather;

+ (instancetype)routeStepWithDistance:(float)distance
                                 name:(NSString *)name
                                state:(NSString *)state
                           travelTime:(NSInteger)travelTime
                              weather:(MTWeatherModel *)weather;

@end


@interface MTRouteDetailStepsData : NSObject

@property (nonatomic) NSArray <MTStepObject *> *stepsObjects;

+ (instancetype)upcomingDisplayDataWithRoutes:(NSArray <MTStepObject *> *)routeSteps;

@end

