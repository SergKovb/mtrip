//
//  MTRouteDetailPresenter.m
//  MTrip
//
#import "MTRouteDetailPresenter.h"
#import "MTWeatherModel.h"
#import "MTRouteItem.h"
#import <MapKit/MapKit.h>


@interface MTRouteDetailPresenter ()
@property (nonatomic) MTRouteItem *routeItem;
@end

@implementation MTRouteDetailPresenter

#pragma mark MTRouteDetailInteractorOutput

- (void)didLoadRouteDetailStepsData:(MTRouteDetailStepsData *)routeStepsData {
    [_userInterface showRouteDetailStepsData:routeStepsData];
}

- (void)didLoadDestinationName:(NSString *)name
                         state:(NSString *)state {
    [_userInterface showDestinationName:name
                                  state:state];
}

- (void)didLoadCurrentWeather:(MTWeatherModel *)weather {
    [_userInterface showCurrentWeather:weather];
}

- (void)didLoadArrivalTimeWeather:(MTWeatherModel *)weather {
    [_userInterface showaArrivalTimeWeather:weather];
}


- (void)didObtainTravelTimeString:(NSString *)travelTimeString {
    [_userInterface showExpectedTimeString:travelTimeString];
}

#pragma mark MTRouteDetailViewOutput

- (void)updateView {
    [_userInterface showRouteItem:_routeItem];
}

#pragma mark MTRouteDetailInput

- (void)configureForRouteItem:(MTRouteItem *)routeItem {
    _routeItem = routeItem;
    [_routeDetailInteractor obtainRouteDetailsForRouteItem:routeItem];
}

@end

