//
//  MTRouteDetailInput.h
//  MTrip
//

#import <ViperMcFlurry/ViperMcFlurry.h>

@class MTRouteItem;

@protocol MTRouteDetailInput <RamblerViperModuleInput>

- (void)configureForRouteItem:(MTRouteItem *)routeItem;

@end
