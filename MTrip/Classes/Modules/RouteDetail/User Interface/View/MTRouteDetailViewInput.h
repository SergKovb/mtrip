//
//  MTRouteDetailViewInterface.h
//  MTrip
//
#import <Foundation/Foundation.h>

@class MTRouteItem;
@class MTWeatherModel;
@class MTRouteDetailStepsData;

// Defines the public interface that something else can use to drive the user interface
@protocol MTRouteDetailViewInput <NSObject>

- (void)showRouteItem:(MTRouteItem *)routeItem;
- (void)showExpectedTimeString:(NSString *)timeString;
- (void)showDestinationName:(NSString *)name
                      state:(NSString *)state;
- (void)showCurrentWeather:(MTWeatherModel *)weatherModel;
- (void)showaArrivalTimeWeather:(MTWeatherModel *)weatherModel;
- (void)showRouteDetailStepsData:(MTRouteDetailStepsData *)data;

@end
