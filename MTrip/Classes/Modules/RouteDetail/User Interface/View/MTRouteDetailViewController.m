//
//  MTRouteDetailViewController.m
//  MTrip
//

#import "MTRouteDetailViewController.h"
#import "MTRouteItem.h"
#import "MTWeatherModel.h"
#import "MTRouteDetailStepsData.h"
#import "MTRouteDetailDataDisplayManager.h"

@interface MTRouteDetailViewController ()

@property (strong, nonatomic) IBOutlet UIView *headerView;

//Route info
@property (strong, nonatomic) IBOutlet UILabel *routeTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *routeNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *routeDescLabel;
@property (strong, nonatomic) IBOutlet UILabel *routeTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *routeDistanceLabel;

@property (nonatomic, strong) IBOutlet UILabel*              cityLabel;

//currentWeather
@property (nonatomic, strong) IBOutlet UIView*              currentWeatherView;
@property (nonatomic, strong) IBOutlet UIStackView*          currentWeatherIconStackView;
@property (nonatomic, strong) IBOutlet UILabel*              currentTempLabel;
@property (nonatomic, strong) IBOutlet UILabel*              currentTimeLabel;

//arrivalTimeWeather
@property (nonatomic, strong) IBOutlet UIStackView*          arrivalTimeWeatherIconStackView;
@property (nonatomic, strong) IBOutlet UILabel*              arrivalTimeTempLabel;
@property (nonatomic, strong) IBOutlet UILabel*              arrivalTimeLabel;
@property (nonatomic, strong) IBOutlet UIView*               arrivalTimeWeatherView;

//stackview subviews

@property (nonatomic, strong) IBOutlet UITableView*         strongTableView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView*         indicator;


@end

@implementation MTRouteDetailViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    _strongTableView.bounces = NO;
    [self configureView];
    [self showNoWeatherMessage];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.output updateView];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self sizeHeaderToFit];
}

#pragma mark MTRouteDetailViewInput

- (void)showNoWeatherMessage {
//to-do
}

- (void)showRouteItem:(MTRouteItem *)routeItem {
    _routeTitleLabel.text = routeItem.name;
    _routeDistanceLabel.text = [NSString stringWithFormat:@"%.0f км",routeItem.distance/1000];
    _routeNameLabel.hidden = YES;
    _routeTimeLabel.hidden = YES;
    _routeDescLabel.hidden = YES;
}

- (void)showExpectedTimeString:(NSString *)timeString {
    _routeTimeLabel.text = timeString;
    _routeTimeLabel.hidden = NO;
}

- (void)showDestinationName:(NSString *)name
                      state:(NSString *)state {
    _routeNameLabel.text = name;
    _routeNameLabel.hidden = NO;
    _routeDescLabel.text = state;
    _routeDescLabel.hidden = NO;
}

- (void)reloadEntries {
    [_strongTableView reloadData];
}

- (void)showRouteDetailStepsData:(MTRouteDetailStepsData *)data {
    [self.indicator stopAnimating];
    [self.dataDisplayManager configureDataDisplayManagerWithRouteSteps:data.stepsObjects];
    [self reloadEntries];
}

- (void)showCurrentWeather:(MTWeatherModel *)weatherModel {
    _currentWeatherView.hidden = NO;
    _cityLabel.text = weatherModel.cityName;
    _currentTempLabel.text = [weatherModel temperatureString];
    _currentTimeLabel.text = [weatherModel timeString];
    foreach(weatherItem, weatherModel.weatherItems) {
        let imageView = [[UIImageView alloc] init];
        imageView.backgroundColor = [UIColor clearColor];
        [imageView.heightAnchor constraintEqualToConstant:80].active = true;
        [imageView.widthAnchor constraintEqualToConstant:80].active = true;
        [_currentWeatherIconStackView addArrangedSubview:imageView];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            let imgURL = [weatherItem urlForIcon];
            let data = [NSData dataWithContentsOfURL:imgURL];
            dispatch_async(dispatch_get_main_queue(), ^{
                [imageView setImage:[UIImage imageWithData:data]];
            });
        });
    }
    _currentWeatherIconStackView.translatesAutoresizingMaskIntoConstraints = false;
}

- (void)showaArrivalTimeWeather:(MTWeatherModel *)weatherModel {
    _arrivalTimeWeatherView.hidden = NO;
    _arrivalTimeTempLabel.text = [weatherModel temperatureString];
    _arrivalTimeLabel.text = [weatherModel timeString];
    foreach(weatherItem, weatherModel.weatherItems) {
        let imageView = [[UIImageView alloc] init];
        imageView.backgroundColor = [UIColor clearColor];
        [imageView.heightAnchor constraintEqualToConstant:80].active = true;
        [imageView.widthAnchor constraintEqualToConstant:80].active = true;
        [_arrivalTimeWeatherIconStackView addArrangedSubview:imageView];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            let imgURL = [weatherItem urlForIcon];
            let data = [NSData dataWithContentsOfURL:imgURL];
            dispatch_async(dispatch_get_main_queue(), ^{
                [imageView setImage:[UIImage imageWithData:data]];
            });
        });
    }
    _arrivalTimeWeatherIconStackView.translatesAutoresizingMaskIntoConstraints = false;
}

#pragma mark - Private methods

- (void)configureView {
    self.strongTableView.dataSource = [self.dataDisplayManager dataSourceForTableView:self.strongTableView];
    self.strongTableView.delegate = [self.dataDisplayManager delegateForTableView:self.strongTableView];
    [self.indicator startAnimating];
}

- (void)sizeHeaderToFit {
    let header = self.strongTableView.tableHeaderView;
    var frame = header.frame;
    frame.size.width = self.strongTableView.frame.size.width;
    header.frame = frame;
    
    [header setNeedsLayout];
    [header layoutIfNeeded];
    let height = [header systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    var headerframe = header.frame;
    
    headerframe.size.height = height;
    header.frame = headerframe;
    self.strongTableView.tableHeaderView = header;
}

@end
