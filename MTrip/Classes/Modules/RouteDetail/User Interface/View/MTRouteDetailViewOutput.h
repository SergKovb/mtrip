//
//  MTRouteDetailModuleInterface.h
//  MTrip
//

#import <Foundation/Foundation.h>

@protocol MTRouteDetailViewOutput <NSObject>

- (void)updateView;

@end
