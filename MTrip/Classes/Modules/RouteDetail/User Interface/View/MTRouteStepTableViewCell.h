//
//  MTRouteStepTableViewCell.h
//  MTrip
//


#import <UIKit/UIKit.h>

@interface MTRouteStepTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *name;
@property (nonatomic, weak) IBOutlet UILabel *travelTime;
@property (nonatomic, weak) IBOutlet UILabel *state;
@property (nonatomic, weak) IBOutlet UILabel *temp;
@property (nonatomic, weak) IBOutlet UILabel *distance;
@property (nonatomic, weak) IBOutlet UIImageView *weatherIcon;
@end
