//
//  MTRouteDetailViewController.h
//  MTrip
//
#import <UIKit/UIKit.h>

#import "MTRouteDetailViewInput.h"
#import "MTRouteDetailViewOutput.h"

@class MTRouteDetailDataDisplayManager;

@interface MTRouteDetailViewController : UIViewController <MTRouteDetailViewInput>

@property (nonatomic, strong) id<MTRouteDetailViewOutput>    output;
@property (nonatomic, strong) MTRouteDetailDataDisplayManager *dataDisplayManager;

@end
