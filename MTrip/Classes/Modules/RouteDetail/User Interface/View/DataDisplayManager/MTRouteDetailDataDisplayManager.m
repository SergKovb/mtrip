//
//  MTRouteDetailDataDisplaManager.m
//  MTrip
//

#import "MTRouteDetailDataDisplayManager.h"
#import "MTRouteDetailStepsData.h"
#import "MTRouteStepTableViewCell.h"
#import "MTTimeFormatterImplementation.h"
#import "MTWeatherModel.h"

static NSString* const MTRouteStepCellIdentifier = @"MTRouteStepCell";

@interface MTRouteDetailDataDisplayManager () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray <MTStepObject *> *routeSteps;

@end

@implementation MTRouteDetailDataDisplayManager

- (void)configureDataDisplayManagerWithRouteSteps:(NSArray<MTStepObject *> *)routeSteps {
    self.routeSteps = routeSteps;
}

#pragma mark DataDisplayManager

- (id<UITableViewDataSource>)dataSourceForTableView:(UITableView *)tableView {
    return self;
}

- (id<UITableViewDelegate>)delegateForTableView:(UITableView *)tableView {
    return self;
}

#pragma mark UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    let routeStepsItem = self.routeSteps[indexPath.row];
    let cell = (MTRouteStepTableViewCell *)[tableView dequeueReusableCellWithIdentifier:MTRouteStepCellIdentifier forIndexPath:indexPath];
    cell.name.text = routeStepsItem.name;
    cell.state.text = routeStepsItem.state;
    cell.temp.text = [routeStepsItem.weather temperatureString];
    cell.distance.text = [NSString stringWithFormat:@"%.0f км",routeStepsItem.distance/1000];
    cell.travelTime.text = [self.timeFormatter stringFromTravelTimeInterval:routeStepsItem.travelTime];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        let weatherItem = routeStepsItem.weather.weatherItems.firstObject;
        let data = [NSData dataWithContentsOfURL: [weatherItem urlForIcon]];
        dispatch_async(dispatch_get_main_queue(), ^{
            [cell.weatherIcon setImage:[UIImage imageWithData:data]];
        });
    });
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.routeSteps count];
}

@end
