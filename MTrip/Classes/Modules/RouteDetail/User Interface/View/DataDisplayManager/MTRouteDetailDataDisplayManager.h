//
//  MTRouteDetailDataDisplaManager.h
//  MTrip
//

#import <Foundation/Foundation.h>
#import "DataDisplayManager.h"

NS_ASSUME_NONNULL_BEGIN

@class MTStepObject;
@class MTTimeFormatterImplementation;

@protocol MTRouteDetailDataDisplayManagerDelegate <NSObject>

- (void)didTapRouteCellWithRouteItem:(MTStepObject *)routeStep;

@end


@interface MTRouteDetailDataDisplayManager : NSObject <DataDisplayManager>

@property (nonatomic, weak) id <MTRouteDetailDataDisplayManagerDelegate> delegate;
@property (nonatomic, strong) MTTimeFormatterImplementation *timeFormatter;

- (void)configureDataDisplayManagerWithRouteSteps:(NSArray <MTStepObject *> *)routeSteps;

@end

NS_ASSUME_NONNULL_END
