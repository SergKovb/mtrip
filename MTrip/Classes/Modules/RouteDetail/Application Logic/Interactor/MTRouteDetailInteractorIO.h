//
//  MTrouteDetailInteratorIO.h
//  MTrip
//

#import <Foundation/Foundation.h>
@class CLLocation;
@class MKRoute;
@class MTWeatherModel;
@class MTRouteDetailStepsData;
@class MTRouteItem;

@protocol MTRouteDetailInteractorInput <NSObject>

- (void)obtainRouteDetailsForRouteItem:(MTRouteItem *)routeItem;

@end


@protocol MTRouteDetailInteractorOutput <NSObject>

- (void)didLoadCurrentWeather:(MTWeatherModel *)weather;
- (void)didLoadArrivalTimeWeather:(MTWeatherModel *)weather;
- (void)didLoadDestinationName:(NSString *)name
                         state:(NSString *)state;
- (void)didLoadRouteDetailStepsData:(MTRouteDetailStepsData *)routeStepsData;
- (void)didObtainTravelTimeString:(NSString *)travelTimeString;

@end
