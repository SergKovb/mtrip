//
//  MTRouteDetailInterator.h
//  MTrip
//

#import <Foundation/Foundation.h>

#import "MTRouteDetailInteractorIO.h"

@class MTRouteDetailDataManager;
@class MTStartLocationManager;
@protocol MTReverseGeocodeService;
@protocol MTRouteFinderService;
@protocol MTWeatherLoaderService;
@protocol MTTimeFormatterService;
@protocol MTWeatherForecastLoaderService;
@protocol MTWeatherForecastFilterService;

@interface MTRouteDetailInteractor : NSObject <MTRouteDetailInteractorInput>

@property (nonatomic, weak)     id <MTRouteDetailInteractorOutput> output;
@property (nonatomic, strong)   id <MTReverseGeocodeService> reverseGeocodeService;
@property (nonatomic, strong)   id <MTRouteFinderService> routeFinderService;
@property (nonatomic, strong)   id <MTWeatherLoaderService> weatherLoaderService;
@property (nonatomic, strong)   id <MTWeatherForecastLoaderService> weatherForecastLoaderService;
@property (nonatomic, strong)   id <MTTimeFormatterService> timeFormatterService;
@property (nonatomic, strong)   id <MTWeatherForecastFilterService> weatherForecastFilterService;

- (instancetype)initWithStartLocationManager:(MTStartLocationManager *)startLocationManager;

@end
