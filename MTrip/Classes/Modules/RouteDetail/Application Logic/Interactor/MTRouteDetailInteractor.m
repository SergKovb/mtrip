//
//  MTRouteDetailInterator.m
//  MTrip
//

#import "MTRouteDetailInteractor.h"
#import "MTWeatherLoaderService.h"
#import "MTRouteFinderService.h"
#import "MTStartLocationManager.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKDirectionsResponse.h>
#import <MapKit/MKPolyline.h>
#import "MTRouteDetailStepsData.h"
#import <ReactiveObjC.h>
#import "MTRouteItem.h"
#import "MTRouteStep.h"
#import "MTReverseGeocodeService.h"
#import "MTTimeFormatterService.h"
#import "MTWeatherForecastLoaderService.h"
#import "MTWeatherForecastModel.h"
#import "MTWeatherForecastFilterService.h"


@interface MTRouteDetailInteractor()

@property (nonatomic, strong)   MTRouteDetailDataManager        *dataManager;
@property (nonatomic, strong)   MTStartLocationManager          *startLocationManager;

@end

@implementation MTRouteDetailInteractor

- (instancetype)initWithStartLocationManager:(MTStartLocationManager *)startLocationManager {
    if ((self = [super init])) {
        _startLocationManager = startLocationManager;
    }
    return self;
}

#pragma mark MTRouteDetailInteractorInput

- (void)obtainRouteDetailsForRouteItem:(MTRouteItem *)routeItem {
    [self loadGeocodeFor:routeItem.destination];
    [self loadWeatherForLocation:routeItem.destination];
    [self obtainRouteStepsDetailsForRouteSteps:routeItem.routeSteps];
    [self obtainDirectionToRouteItem:routeItem];
}

#pragma mark Private methods

- (void)loadWeatherForLocation:(CLLocation *)location {
    NSLog(@"loadWeatherForLocation %@",location);
    __weak typeof(self) weakSelf = self;
    [self.weatherLoaderService loadCurrentWeatherForLocation:location block:^(MTWeatherModel *weatherModel) {
        if (!weakSelf) {
            return;
        }
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf.output didLoadCurrentWeather:weatherModel];
    }];
}

- (RACSignal *)signalForLoadGeocode:(CLLocation *)location {
    let noGeocodeError = [NSError errorWithDomain:@"Geocode"
                                             code:1
                                         userInfo:nil];
    __weak typeof(self) weakSelf = self;
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf.reverseGeocodeService loadAppleReverseGeocodeWithLocation:location
                                                                  block:^(CLPlacemark *placemark) {
                                                                      [subscriber sendNext:placemark];
                                                                  } failure:^{
                                                                      [subscriber sendError:noGeocodeError];
                                                                  }];
        return nil;
    }];
}

- (void)obtainDirectionToRouteItem:(MTRouteItem *)routeItem {
    __weak typeof(self) weakSelf = self;
    [_routeFinderService routeFrom:_startLocationManager.startLocation
                                to:routeItem.destination
                           success:^(MKRoute *route) {
                               if (!weakSelf) {
                                   return;
                               }
                               __strong __typeof(weakSelf) strongSelf = weakSelf;
                               [strongSelf.output didObtainTravelTimeString:[self.timeFormatterService stringFromTravelTimeInterval:route.expectedTravelTime]];
                               let arrivalWeatherSignal = [strongSelf signalForLoadWeather:routeItem.destination
                                                                              timeInterval:route.expectedTravelTime];
                               [arrivalWeatherSignal subscribeNext:^(MTWeatherModel *weather) {
                                   [weakSelf.output didLoadArrivalTimeWeather:weather];
                               }];
                           }];
}

- (void)obtainRouteStepsDetailsForRouteSteps:(NSArray <MTRouteStep *> *)routeSteps {
    let sequence = [routeSteps.rac_sequence map:^id _Nullable(MTRouteStep * _Nullable routeStep) {
        return [self mergeWeatherAndGeocode:[[CLLocation alloc] initWithLatitude:routeStep.lat longitude:routeStep.lon]
                                 travelTime:routeStep.travelTime
                                   distance:routeStep.distance];
    }];
    let combineLatest = [RACSignal combineLatest:sequence];
    
    __weak typeof(self) weakSelf = self;
    [[[combineLatest subscribeOn:[RACScheduler scheduler]]
      deliverOn:[RACScheduler mainThreadScheduler]] subscribeNext:^(RACTuple *tuple){
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        let routeDetailStepsData = [MTRouteDetailStepsData upcomingDisplayDataWithRoutes:tuple.allObjects];
        [strongSelf.output didLoadRouteDetailStepsData:routeDetailStepsData];
    }];
}


- (void)loadGeocodeFor:(CLLocation *)destination {
    [_reverseGeocodeService loadAppleReverseGeocodeWithLocation:destination
                                                          block:^(CLPlacemark *placemark) {
                                                              [self.output didLoadDestinationName:placemark.name
                                                                                            state:placemark.locality];
                                                          } failure:^{
                                                              //to do
                                                          }];
}

- (RACSignal *)signalForLoadWeather:(CLLocation *)location
                       timeInterval:(NSInteger)timeInterval{
    __weak typeof(self) weakSelf = self;
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf.weatherForecastLoaderService loadWeatherForLocation:location
                                                                  block:^(MTWeatherForecastModel *weatherForecastModel) {
                                                                      let weatherModel = [self.weatherForecastFilterService weatherModelFromForecast:weatherForecastModel forTimeInterval:timeInterval];
                                                                      [subscriber sendNext:weatherModel];
                                                                      [subscriber sendCompleted];
                                                                  } failure:^(NSError *error) {
                                                                      [subscriber sendError:error];
                                                                  }];
        return nil;
    }];
}

- (RACSignal *)mergeWeatherAndGeocode:(CLLocation *)location
                           travelTime:(NSInteger)travelTime
                             distance:(float)distance{
    let merge = [RACSignal combineLatest:@[ [self signalForLoadGeocode:location], [self signalForLoadWeather:location
                                                                                                timeInterval:travelTime]]
                                  reduce:^id (CLPlacemark *placemark, MTWeatherModel *weather) {
                                      let routeStep = [MTStepObject routeStepWithDistance:distance
                                                                                     name:placemark.name
                                                                                    state:placemark.country
                                                                               travelTime:travelTime
                                                                                  weather:weather];
                                      return routeStep;
                                  }];
    return merge;
}

@end

