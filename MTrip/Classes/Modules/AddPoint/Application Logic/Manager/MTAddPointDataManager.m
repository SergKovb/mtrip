//
//  MTAddPointDataManager.m
//  MTrip
//

#import "MTAddPointDataManager.h"
#import "MTRoutesDataStore.h"
#import "MTRouteStep.h"
#import "MTManagedRouteStep.h"
#import "MTRouteItem.h"
#import "MTManagedRouteItem.h"

@implementation MTAddPointDataManager

- (void)addNewRoute:(MTRouteItem *)route {
    let newRoute = [self.dataStore newRouteItem];
    newRoute.name = route.name;
    newRoute.lat = route.lat;
    newRoute.lon = route.lon;
    newRoute.distance = route.distance;
    let managedRouteSteps = (NSMutableOrderedSet<MTManagedRouteStep *> *) [NSMutableOrderedSet new];
    foreach(routeStep, route.routeSteps) {
        let managedRouteStep = [self.dataStore newRouteStep];
        managedRouteStep.lat = routeStep.lat;
        managedRouteStep.lon = routeStep.lon;
        managedRouteStep.distance = routeStep.distance;
        managedRouteStep.travelTime = routeStep.travelTime;
        managedRouteStep.routeItem = newRoute;
        [managedRouteSteps addObject:managedRouteStep];
    }
    newRoute.routeSteps = [managedRouteSteps copy];
    [self.dataStore save];
}

@end
