//
//  MTAddPointDataManager.h
//  MTrip
//

#import <Foundation/Foundation.h>


@class MTRoutesDataStore;
@class MTRouteItem;

@interface MTAddPointDataManager : NSObject

@property (nonatomic, strong) MTRoutesDataStore *dataStore;

- (void)addNewRoute:(MTRouteItem *)route;

@end
