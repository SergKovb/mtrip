//
//  MTAddPointInteratorIO.h
//  MTrip
//

#import <Foundation/Foundation.h>

@class MTGeoObject;
@class CLLocation;
@class MKRoute;

@protocol MTAddPointInteractorInput <NSObject>

- (void)saveStartPointLocation:(CLLocation *)location;
- (void)loadLocationNameAndDirectionsToLocation:(CLLocation *)location;
- (void)loadLocationNameWithLocation:(CLLocation *)location;
- (void)saveNewEntryWithGeoObject:(MTGeoObject *)geoObject
                            route:(MKRoute *)route;

@end


@protocol MTAddPointInteractorOutput <NSObject>

- (void)didLoadGeoObject:(MTGeoObject *)geoObject;
- (void)didLoadRoute:(MKRoute *)route;
- (void)didSaveRoute;

@end
