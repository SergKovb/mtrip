//
//  MTAddPointInterator.m
//  MTrip
//

#define DISTANCE 100000


#import "MTAddPointInteractor.h"
#import "MTAddPointDataManager.h"
#import <AFNetworking.h>
#import "MTGeocodeItem.h"
#import "MTRouteFinderService.h"
#import "MTStartLocationManager.h"
#import <ReactiveObjC.h>
#import "MTReverseGeocodeService.h"
#import "MTRouteFilterService.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKDirectionsResponse.h>
#import "MTRouteStep.h"
#import "MTRouteItem.h"


@interface MTAddPointInteractor()

@property (nonatomic, strong)   MTAddPointDataManager   *dataManager;
@property (nonatomic, strong)   MTStartLocationManager  *startLocationManager;

@end


@implementation MTAddPointInteractor

- (instancetype)initWithDataManager:(MTAddPointDataManager *)dataManager
               startLocationManager:(MTStartLocationManager *)startLocationManager {
    if ((self = [super init])) {
        _dataManager = dataManager;
        _startLocationManager = startLocationManager;
    }
    
    return self;
}

- (void)saveStartPointLocation:(CLLocation *)location {
    [_startLocationManager setStartLocation:location];
}

- (void)loadLocationNameWithLocation:(CLLocation *)location {
    __weak typeof(self) weakSelf = self;
    [[self signalForLocationName:location] subscribeNext:^(MTGeoObject *geoObject) {
        if (!weakSelf) {
            return;
        }
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf.output didLoadGeoObject:geoObject];
    }];
}

- (void)loadLocationNameAndDirectionsToLocation:(CLLocation *)location {
    __weak typeof(self) weakSelf = self;
    let merge = [RACSignal combineLatest:@[ [self signalForDirections:location], [self signalForLocationName:location]]
                                  reduce:^id (MKRoute *route, MTGeoObject *geoObject) {
                                      return [NSDictionary dictionaryWithObjectsAndKeys:route, @"route",
                                              geoObject, @"geoObject",nil];
                                  }];
    [merge subscribeNext:^(NSDictionary *dict){
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf.output didLoadRoute:dict[@"route"]];
        [strongSelf.output didLoadGeoObject:dict[@"geoObject"]];
    }];
}

- (RACSignal *)signalForDirections:(CLLocation *)location {
    __weak typeof(self) weakSelf = self;
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        [self.routeFinderService routeFrom:strongSelf.startLocationManager.startLocation
                                        to:location
                                   success:^(MKRoute *route) {
                                       [subscriber sendNext:route];
                                       [subscriber sendCompleted];
                                   } failure:^(NSError *error) {
                                       [subscriber sendError:error];
                                   }];
        return nil;
    }];
}

- (RACSignal *)signalForLocationName:(CLLocation *)location {
    let noLocationNameError = [NSError errorWithDomain:@"LocationName"
                                                  code:1
                                              userInfo:nil];
    __weak typeof(self) weakSelf = self;
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf.reverseGeocodeService loadYandexReverseGeocodeWithLocation:location
                                                         block:^(MTGeoObject *geoObject) {
                                                             [subscriber sendNext:geoObject];
                                                             [subscriber sendCompleted];                                                         }failure:^{
                                                                 [subscriber sendError:noLocationNameError];
                                                             }];
        return nil;
    }];
}

- (void)saveNewEntryWithGeoObject:(MTGeoObject *)geoObject
                            route:(MKRoute *)route {
    __block let routeItem = [MTRouteItem routeItemWithName:geoObject.name
                                                       lat:geoObject.lat
                                                       lon:geoObject.lon];
    let filteredArray = [self.routeFilterService filterSteps:route.steps
                                                  byDistance:DISTANCE];
    __weak typeof(self) weakSelf = self;
    let sequence = [filteredArray.rac_sequence map:^id _Nullable(MKRouteStep *routeStep) {
        let signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            let location = [[CLLocation alloc] initWithLatitude:routeStep.polyline.coordinate.latitude
                                                      longitude:routeStep.polyline.coordinate.longitude];
            [strongSelf.routeFinderService routeFrom:strongSelf.startLocationManager.startLocation
                                            to:location
                                       success:^(MKRoute *route) {
                                           [subscriber sendNext:route];
                                           [subscriber sendCompleted];
                                       } failure:^(NSError *error) {
                                           [subscriber sendError:error];
                                       }];
            return nil;
        }];
        return signal;
    }];
    
    let combineLatest = [RACSignal combineLatest:sequence];
    routeItem.distance = route.distance;

    [[[combineLatest subscribeOn:[RACScheduler scheduler]]
      deliverOn:[RACScheduler mainThreadScheduler]]
     subscribeNext:^(RACTuple *tuple) {
         __strong __typeof(weakSelf) strongSelf = weakSelf;
         let routes = (NSArray <MKRoute *> *)tuple.allObjects;
         let routeSteps = (NSMutableArray <MTRouteStep *> *)[NSMutableArray new];
         foreach(route, routes) {
             let routeStep = [MTRouteStep routeStepWithRoute:route];
             [routeSteps addObject:routeStep];
         }
         [routeItem setRouteSteps:[routeSteps copy]];
         [strongSelf.dataManager addNewRoute:routeItem];
         [strongSelf.output didSaveRoute];
     }];

}

@end

