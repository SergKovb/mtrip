//
//  MTAddPointInterator.h
//  MTrip
//

#import <Foundation/Foundation.h>

#import "MTAddPointInteractorIO.h"

@class MTAddPointDataManager;
@class MTStartLocationManager;
@protocol MTReverseGeocodeService;
@protocol MTRouteFinderService;
@protocol MTRouteFilterService;

@interface MTAddPointInteractor : NSObject <MTAddPointInteractorInput>

@property (nonatomic, weak)     id <MTAddPointInteractorOutput> output;
@property (nonatomic, strong)   id <MTReverseGeocodeService> reverseGeocodeService;
@property (nonatomic, strong)   id <MTRouteFinderService> routeFinderService;
@property (nonatomic, strong)   id <MTRouteFilterService> routeFilterService;

- (instancetype)initWithDataManager:(MTAddPointDataManager *)dataManager
               startLocationManager:(MTStartLocationManager *)startLocationManager;

@end
