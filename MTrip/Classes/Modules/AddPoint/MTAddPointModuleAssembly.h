//
//  MTAddPointModuleAssembly.h
//  MTrip
//


#import "TyphoonAssembly.h"

NS_ASSUME_NONNULL_BEGIN

@protocol RamblerViperModuleFactoryProtocol;

@interface MTAddPointModuleAssembly : TyphoonAssembly

- (id<RamblerViperModuleFactoryProtocol>)factoryAddPointModule;

@end

NS_ASSUME_NONNULL_END
