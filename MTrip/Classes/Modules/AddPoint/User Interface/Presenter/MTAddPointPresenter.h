//
//  MTAddPointPresenter.h
//  MTrip
//

#import <Foundation/Foundation.h>
#import "MTAddPointViewOutput.h"
#import "MTAddPointInteractorIO.h"
#import "MTAddPointRouterInput.h"
#import "MTAddPointViewInput.h"
#import "MTAddPointInput.h"

@protocol MTAddPointViewInput;
@protocol MTAddModuleDelegate;

@interface MTAddPointPresenter : NSObject <MTAddPointViewOutput, MTAddPointInteractorOutput, MTAddPointInput>


@property (nonatomic, strong) id<MTAddPointInteractorInput>     addPointInteractor;
@property (nonatomic, strong) id<MTAddPointRouterInput>         addPointRouter;
@property (nonatomic, weak) id <MTAddPointViewInput>            userInterface;

@end

