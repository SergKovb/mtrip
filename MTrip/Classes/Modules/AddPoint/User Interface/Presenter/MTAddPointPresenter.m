//
//  MTAddPointPresenter.m
//  MTrip
//

#import "MTAddPointPresenter.h"
#import "MTGeocodeItem.h"
#import "MTRouteFinderService.h"
#import <MapKit/MKDirectionsResponse.h>
#import "MTStartLocationManager.h"

@interface MTAddPointPresenter ()
@property (nonatomic) CLLocation *location;
@property MTGeoObject *geoObject;
@property MKRoute *route;
@property BOOL isStartLocation;
@end

@implementation MTAddPointPresenter

- (void)configureForAddPoint {
    _isStartLocation = NO;
}

- (void)configureForAddStartPoint {
    _isStartLocation = YES;
}

- (void)updateView {
    if (_isStartLocation) {
        [self.userInterface showStartLocationView];
    } else {
        [self.userInterface showRouteLocationView];
    }
}

- (void)setSelectedLocation:(CLLocation *)location {
    _location = location;
}

- (void)save {
    if (_isStartLocation) {
        [self saveStartLocation];
    } else {
        [self saveRoute];
    }
}

- (void)saveStartLocation {
    [_addPointInteractor saveStartPointLocation:_location];
}

- (void)saveRoute {
    //to-do show allert or progress
    [self.addPointInteractor saveNewEntryWithGeoObject:_geoObject
                                                 route:_route];
}

- (void)didSaveRoute {
    NSLog(@"MTAddPointPresenter -> didSaveRoute");
    [self.userInterface dismiss];
}

- (void)didSelectedPointLocation:(CLLocation *)location
                    userLocation:(CLLocation *)userLocation {
    [self.addPointInteractor loadLocationNameAndDirectionsToLocation:location];
}

- (void)didSelectedStartLocation:(CLLocation *)location {
    _location = location;
    [self.addPointInteractor loadLocationNameWithLocation:location];
}

- (void)cancelAddAction {
}


- (void)didLoadRoute:(MKRoute *)route {
    _route = route;
    [self.userInterface showRoute:_route];
}

- (void)didLoadGeoObject:(MTGeoObject *)geoObject {
    _geoObject = geoObject;
    [self.userInterface showLocationWithName:geoObject.name desc:geoObject.desc];
}

@end
