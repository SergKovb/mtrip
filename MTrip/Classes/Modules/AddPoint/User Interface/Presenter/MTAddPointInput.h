//
//  MTAddPointInput.h
//  MTrip
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>


@protocol MTAddPointInput <RamblerViperModuleInput>

- (void)configureForAddStartPoint;
- (void)configureForAddPoint;

@end
