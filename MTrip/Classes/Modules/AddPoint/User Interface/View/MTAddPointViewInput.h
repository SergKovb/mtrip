//
//  MTAddPointViewInterface.h
//  MTrip
//


#import <Foundation/Foundation.h>
@class MKRoute;

@protocol MTAddPointViewInput <NSObject>

- (void)showStartLocationView;

- (void)showRouteLocationView;

- (void)showLocationWithName:(NSString *)name
                        desc:(NSString *)desc;

- (void)showRoute:(MKRoute *)route;
- (void)dismiss;

@end
