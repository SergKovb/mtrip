//
//  MTAddPointModuleInterface.h
//  MTrip

@class  CLLocation;
@protocol MTAddPointViewOutput <NSObject>

- (void)cancelAddAction;
- (void)didSelectedPointLocation:(CLLocation *)location
                    userLocation:(CLLocation *)userLocation;
- (void)didSelectedStartLocation:(CLLocation *)location;
- (void)save;

- (void)updateView;

@end
