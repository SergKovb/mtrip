//
//  MTAddPointViewController.m
//  MTrip
//

#import "MTAddPointViewController.h"
#import <MapKit/MapKit.h>


@interface MTAddPointViewController () <MKMapViewDelegate>
@property (nonatomic, weak) IBOutlet MKMapView *mapView;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet UIView *targetConfirmView;
@property (strong, nonatomic) IBOutlet UIView *locationView;
@property MKPointAnnotation *currentAnnotation;
@property UITapGestureRecognizer *tapGestureRecognizer;

@property (nonatomic) BOOL isTargetConfirmViewShown;
@property (nonatomic) BOOL isLocationTitleLoaded;

//targetConfirmView
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UILabel *descLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UIButton *confirmButton;
@property (strong, nonatomic) IBOutlet UIButton *canselButton;

@end

@implementation MTAddPointViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.barTintColor = [UIColor orangeColor];
    [self configureMapView];
    [self.output updateView];
}

- (void)showStartLocationView {
    self.navigationItem.title = @"Точка отправления";
    _mapView.showsUserLocation = false;
    let longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                   action:@selector(handleLongPressGestureStartLocation:)];
    [_mapView addGestureRecognizer:longPressGestureRecognizer];
}

- (void)showRouteLocationView {
    self.navigationItem.title = @"Маршруты";
    _mapView.showsUserLocation = true;
    let longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                   action:@selector(handleLongPressGestureDestinationLocation:)];
    [_mapView addGestureRecognizer:longPressGestureRecognizer];
}

- (void)configureMapView {
    _mapView.delegate = self;
    [_mapView setMapType:MKMapTypeStandard];
    [_mapView setZoomEnabled:YES];
    [_mapView setScrollEnabled:YES];
    _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                    action:@selector(handlePressGesture:)];
    _tapGestureRecognizer.enabled = NO;
    [self.view addGestureRecognizer:_tapGestureRecognizer];
}

- (void)handleLongPressGestureStartLocation:(UIGestureRecognizer*)sender {
    if (!_isTargetConfirmViewShown) {
        let touchPoint = [sender locationInView:_mapView];
        let coordinate = [_mapView convertPoint:touchPoint toCoordinateFromView:_mapView];
        _currentAnnotation = [MKPointAnnotation new];
        _currentAnnotation.coordinate = coordinate;
        let location = [[CLLocation alloc] initWithLatitude:coordinate.latitude
                                                          longitude:coordinate.longitude];
        [self.output didSelectedStartLocation:location];
        [self setIsTargetConfirmViewShown:YES];
    }
}

- (void)handleLongPressGestureDestinationLocation:(UIGestureRecognizer*)sender {
    if (!_isTargetConfirmViewShown) {
        let touchPoint = [sender locationInView:_mapView];
        let coordinate = [_mapView convertPoint:touchPoint toCoordinateFromView:_mapView];
        _currentAnnotation = [MKPointAnnotation new];
        _currentAnnotation.coordinate = coordinate;
        let location = [[CLLocation alloc] initWithLatitude:coordinate.latitude
                                                          longitude:coordinate.longitude];
        [self.output didSelectedPointLocation:location
                                       userLocation:[_mapView userLocation].location];
        [self setIsTargetConfirmViewShown:YES];
    }
}

- (void)handlePressGesture:(UIGestureRecognizer*)sender {
    [self hideTargetConfirmView:self];
}

- (void)setIsTargetConfirmViewShown:(BOOL)isTargetConfirmViewShown {
    _isTargetConfirmViewShown = isTargetConfirmViewShown;
    if (isTargetConfirmViewShown) {
        [_mapView addAnnotation:_currentAnnotation];
        [self showTargetConfirmView];
        [self setIsLocationTitleLoaded:NO];
    } else {
        [_targetConfirmView removeFromSuperview];
        [_mapView removeOverlays:self.mapView.overlays];
        [_mapView removeAnnotation:_currentAnnotation];
    }
    _tapGestureRecognizer.enabled = isTargetConfirmViewShown;
}

- (void)setIsLocationTitleLoaded:(BOOL)isLocationTitleLoaded {
    _isLocationTitleLoaded = isLocationTitleLoaded;
    if (_isLocationTitleLoaded) {
        [_activityIndicator stopAnimating];
    } else {
        [_activityIndicator startAnimating];
    }
    _locationView.hidden = !_isLocationTitleLoaded;
}

- (IBAction)hideTargetConfirmView:(id)sender {
    [self setIsTargetConfirmViewShown:NO];
}

- (IBAction)toUsersLocation {
    let locationCoordinate = [[[_mapView userLocation] location] coordinate];
    var region = _mapView.region;
    region.center.latitude = locationCoordinate.latitude;
    region.center.longitude = locationCoordinate.longitude;
    [_mapView setRegion:region animated:NO];
}

- (IBAction)zoomIn {
    var region = _mapView.region;
    region.span.latitudeDelta /= 2;
    region.span.longitudeDelta /= 2;
    [_mapView setRegion:region animated:NO];
}

- (IBAction)zoomOut {
    var region = _mapView.region;
    region.span.latitudeDelta = MIN(region.span.latitudeDelta * 2.0, 180.0);
    region.span.longitudeDelta *= MIN(region.span.longitudeDelta * 2.0, 180.0);
    [_mapView setRegion:region animated:NO];
}

- (IBAction)save:(id)sender {
   // [self.navigationController popViewControllerAnimated:YES];
    //to-do закрытие контроллера
    [self.output save];
}


- (IBAction)cancel:(id)sender {
    [self.output cancelAddAction];
}

- (void)dismiss {
     [self.navigationController popViewControllerAnimated:YES];
}

- (void)showTargetConfirmView {
    [self.view addSubview:_targetConfirmView];
    _targetConfirmView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addConstraints: [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[botView]-(0)-|"
                                                                       options:0 metrics:nil
                                                                         views:@{@"botView":_targetConfirmView}]];
    let verticalConstraint = [NSLayoutConstraint constraintWithItem:_targetConfirmView
                                                                          attribute:NSLayoutAttributeBottom
                                                                          relatedBy:NSLayoutRelationEqual
                                                                             toItem:self.view
                                                                          attribute:NSLayoutAttributeBottom
                                                                         multiplier:1.0
                                                                           constant:0];
    [self.view addConstraint:verticalConstraint];
    _tapGestureRecognizer.enabled = YES;
}

- (void)showLocationWithName:(NSString *)name
                        desc:(NSString *)desc {
    [self setIsLocationTitleLoaded:YES];
    _nameLabel.text = name;
    _descLabel.text = desc;
}

- (void)showRoute:(MKRoute *)route {
    [_mapView addOverlay:route.polyline];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    let lineView = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    lineView.strokeColor = [UIColor blueColor];
    return lineView;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self applyMapViewMemoryFix];
    
}

- (void)applyMapViewMemoryFix {
    switch (_mapView.mapType) {
        case MKMapTypeHybrid:
        {
            _mapView.mapType = MKMapTypeStandard;
        }
            break;
        case MKMapTypeStandard:
        {
            _mapView.mapType = MKMapTypeHybrid;
        }
            break;
        default:
            break;
    }
    [_mapView removeAnnotations:_mapView.annotations];
    
    foreach(overlay, _mapView.overlays) {
        [_mapView removeOverlay:overlay];
    }
    _mapView.showsUserLocation = NO;
    _mapView.delegate = nil;
    [_mapView removeFromSuperview];
    _mapView = nil;
}

@end
