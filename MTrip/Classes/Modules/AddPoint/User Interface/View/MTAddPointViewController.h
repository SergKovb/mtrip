//
//  MTAddPointViewController.h
//  MTrip
//

#import <UIKit/UIKit.h>

#import "MTAddPointViewInput.h"
#import "MTAddPointViewOutput.h"

@interface MTAddPointViewController : UIViewController <MTAddPointViewInput>

@property (nonatomic, strong) id<MTAddPointViewOutput>    output;

@end
