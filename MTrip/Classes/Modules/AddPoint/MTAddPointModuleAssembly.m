//
//  MTAddPointModuleAssembly.m
//  MTrip
//

#import "MTAddPointModuleAssembly.h"
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "MTStartModuleAssembly.h"
#import "MTServiceComponentAssembly.h"

#import "MTAddPointViewController.h"
#import "MTAddPointInteractor.h"
#import "MTAddPointDataManager.h"
#import "MTAddPointRouter.h"
#import "MTAddPointPresenter.h"
#import "MTStartLocationManager.h"
#import "MTRoutesDataStore.h"



@interface MTAddPointModuleAssembly ()

@property (nonatomic,strong,readonly) MTStartModuleAssembly         *startModuleAssembly;
@property (nonatomic,strong,readonly) MTServiceComponentAssembly    *serviceComponentAssembly;


@end

@implementation MTAddPointModuleAssembly

- (id<RamblerViperModuleFactoryProtocol>)factoryAddPointModule {
    return [TyphoonDefinition withClass:[RamblerViperModuleFactory class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithStoryboard:andRestorationId:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self storyboardAddPointModule]];
                                                  [initializer injectParameterWith:@"MTAddPointViewController"];
                                              }];
                          }];
}

- (UIStoryboard*)storyboardAddPointModule {
    return [TyphoonDefinition withClass:[TyphoonStoryboard class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(storyboardWithName:factory:bundle:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:@"Main"];
                                                  [initializer injectParameterWith:self];
                                                  [initializer injectParameterWith:nil];
                                              }];
                          }];
}

- (MTAddPointViewController *)viewModuleAddPoint {
    return [TyphoonDefinition withClass:[MTAddPointViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterModuleAddPoint]];
                          }];
}

- (MTAddPointPresenter *)presenterModuleAddPoint {
    return [TyphoonDefinition withClass:[MTAddPointPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(userInterface)
                                                    with:[self viewModuleAddPoint]];
                              [definition injectProperty:@selector(addPointInteractor)
                                                    with:[self interactorModuleAddPoint]];
                              [definition injectProperty:@selector(addPointRouter)
                                                    with:[self routerModuleAddPoint]];
                          }];
}

- (MTAddPointInteractor *)interactorModuleAddPoint {
    return [TyphoonDefinition withClass:[MTAddPointInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithDataManager:startLocationManager:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self dataManagerModuleAddPoint]];
                                                  [initializer injectParameterWith:[self.startModuleAssembly locationManagerModuleStart]];
                                              }];
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterModuleAddPoint]];
                              [definition injectProperty:@selector(reverseGeocodeService)
                                                    with:[self.serviceComponentAssembly reverseGeocodeService]];
                              [definition injectProperty:@selector(routeFinderService)
                                                    with:[self.serviceComponentAssembly routeFinderService]];
                              [definition injectProperty:@selector(routeFilterService)
                                                    with:[self.serviceComponentAssembly routeFilterService]];
                          }];
}

- (MTAddPointRouter *)routerModuleAddPoint {
    return [TyphoonDefinition withClass:[MTAddPointRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                          }];
}

- (MTAddPointDataManager *)dataManagerModuleAddPoint {
    return [TyphoonDefinition withClass:[MTAddPointDataManager class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(dataStore)
                                                    with:[MTRoutesDataStore new]];
                          }];
}
@end
