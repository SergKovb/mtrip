//
//  MTStartModuleAssembly.m
//  MTrip
//

#import "MTStartModuleAssembly.h"
#import "MTAddPointModuleAssembly.h"
#import "MTServiceComponentAssembly.h"

#import "MTStartViewController.h"
#import "MTStartInteractor.h"
#import "MTStartDataManager.h"
#import "MTStartRouter.h"
#import "MTStartPresenter.h"
#import "MTStartLocationManager.h"


@interface  MTStartModuleAssembly()

@property (nonatomic,strong,readonly) MTAddPointModuleAssembly      *addPointModuleAssembly;
@property (nonatomic,strong,readonly) MTServiceComponentAssembly    *serviceComponentAssembly;


@end

@implementation MTStartModuleAssembly


- (MTStartPresenter *)presenterModuleStart {
    return [TyphoonDefinition withClass:[MTStartPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(startInteractor)
                                                    with:[self interactorModuleStart]];
                              [definition injectProperty:@selector(startRouter)
                                                    with:[self routerModuleStart]];
                              [definition injectProperty:@selector(userInterface)
                                                    with:[self viewModuleStart]];
                          }];
}

- (MTStartInteractor *)interactorModuleStart {
    return [TyphoonDefinition withClass:[MTStartInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithDataManager:startLocationManager:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self dataManagerModuleStart]];
                                                  [initializer injectParameterWith:[self locationManagerModuleStart]];
                                              }];
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterModuleStart]];
                              [definition injectProperty:@selector(reverseGeocodeService)
                                                    with:[self.serviceComponentAssembly reverseGeocodeService]];
                              [definition injectProperty:@selector(weatherLoaderService)
                                                    with:[self.serviceComponentAssembly weatherLoaderService]];
                              [definition performAfterInjections:@selector(setup)];
                          }];
}

- (MTStartRouter *)routerModuleStart {
    return [TyphoonDefinition withClass:[MTStartRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewModuleStart]];
                          }];
}

- (MTStartDataManager *)dataManagerModuleStart {
    return [TyphoonDefinition withClass:[MTStartDataManager class]
                          configuration:^(TyphoonDefinition *definition) {

                          }];
}

- (MTStartLocationManager *)locationManagerModuleStart {
    return [TyphoonDefinition withClass:[MTStartLocationManager class]
                          configuration:^(TyphoonDefinition *definition) {
                              definition.scope = TyphoonScopeSingleton;
                          }];
}

- (MTStartViewController *)viewModuleStart {
    return [TyphoonDefinition withClass:[MTStartViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterModuleStart]];
                          }];
}

@end
