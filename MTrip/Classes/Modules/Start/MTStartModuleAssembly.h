//
//  MTStartModuleAssembly.h
//  MTrip
//


#import "TyphoonAssembly.h"

NS_ASSUME_NONNULL_BEGIN

@class MTStartLocationManager;

@interface MTStartModuleAssembly : TyphoonAssembly

- (MTStartLocationManager *)locationManagerModuleStart;

@end

NS_ASSUME_NONNULL_END
