//
//  MTStartRouter.h
//  MTrip
//


#import <Foundation/Foundation.h>
#import "MTStartRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;
@protocol RamblerViperModuleFactoryProtocol;

@interface MTStartRouter : NSObject <MTStartRouterInput>

@property (nonatomic,weak) id<RamblerViperModuleTransitionHandlerProtocol>  transitionHandler;
@property (nonatomic,strong) id<RamblerViperModuleFactoryProtocol>          routeDetailModuleFactory;
@property (nonatomic,strong) id<RamblerViperModuleFactoryProtocol>          addPointModuleFactory;

@end

