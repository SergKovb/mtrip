//
//  MTStartRouter.m
//  MTrip
//


#import "MTStartRouter.h"
#import "MTStartPresenter.h"
#import "MTStartViewController.h"
#import "MTAddPointRouter.h"
#import "MTAddPointInput.h"

static NSString* const MTAddPointSegue = @"addStartPoint";

@implementation MTStartRouter

- (void)presentAddStartPointInterface {
    [[self.transitionHandler openModuleUsingSegue:MTAddPointSegue]
     thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<MTAddPointInput> moduleInput) {
         [moduleInput configureForAddStartPoint];
         return nil;
     }];
}

@end
