//
//  MTStartRouterInput.h
//  MTrip
//


@protocol MTStartRouterInput <NSObject>

- (void)presentAddStartPointInterface;

@end
