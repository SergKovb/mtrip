//
//  MTStartPresenter.h
//  MTrip
//

#import <Foundation/Foundation.h>
#import "MTStartViewOuput.h"
#import "MTStartInteractorIO.h"
#import "MTStartRouterInput.h"
#import "MTStartViewInput.h"

@protocol MTStartViewInput;

@interface MTStartPresenter : NSObject <MTStartInteractorOutput, MTStartViewOuput>

@property (nonatomic, strong) id<MTStartInteractorInput>            startInteractor;
@property (nonatomic, strong) id<MTStartRouterInput>                startRouter;
@property (nonatomic, weak) id <MTStartViewInput>                     userInterface;

@end
