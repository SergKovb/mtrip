//
//  MTStartPresenter.m
//  MTrip
//


#import "MTStartPresenter.h"
#import "MTWeatherModel.h"
#import "MTGeocodeItem.h"

@implementation MTStartPresenter

- (void)didLoadCurrentWeather:(MTWeatherModel *)weatherModel {
    if (weatherModel) {
        [_userInterface showCurrentWeather:weatherModel];
    } else {
        [_userInterface showNoWeatherMessage];
    }
}

- (void)updateView {
    [_startInteractor loadGeoObjectForStartLocation];
}

- (void)didLoadGeoObjectForStartLocation:(MTGeoObject *)geoObject {
    [_userInterface showStartLocationViewWithCity:geoObject.administrativeAreaName
                                          address:geoObject.addressLine];
}

- (void)didFailedLoadGeoObjectForStartLocation{
    [_userInterface showNoStartLocationView];
}

- (void)addStartLocation{
    [self.startRouter presentAddStartPointInterface];
}

@end
