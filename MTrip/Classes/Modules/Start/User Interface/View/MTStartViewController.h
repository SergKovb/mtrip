//
//  MTStartViewController.h
//  MTrip
//


#import <UIKit/UIKit.h>
#import "MTStartViewInput.h"
#import "MTStartViewOuput.h"

@interface MTStartViewController : UIViewController <MTStartViewInput>

@property (nonatomic, strong) id<MTStartViewOuput>    output;

@end
