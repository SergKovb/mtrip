//
//  MTStartViewInterface.h
//  MTrip
//


#import <Foundation/Foundation.h>

@class MTWeatherModel;

@protocol MTStartViewInput <NSObject>

- (void)showNoWeatherMessage;
- (void)showCurrentWeather:(MTWeatherModel *)weatherModel;
- (void)showNoStartLocationView;
- (void)showStartLocationViewWithCity:(NSString *)cityName
                               address:(NSString *)address;

@end
