//
//  MTStartViewController.m
//  MTrip
//

#import "MTStartViewController.h"
#import "MTWeatherModel.h"


@interface MTStartViewController ()

//start location
@property (nonatomic, strong) IBOutlet UIView*              noLocationView;
@property (nonatomic, strong) IBOutlet UIView*              locationView;
@property (nonatomic, strong) IBOutlet UILabel*              startTitle;
@property (nonatomic, strong) IBOutlet UILabel*              startSubtitle;

//current weather
@property (nonatomic, strong) IBOutlet UIView*              noContentView;
@property (nonatomic, strong) IBOutlet UIView*              currentWeatherView;

//currentWeather
@property (nonatomic, strong) IBOutlet UILabel*              cityLabel;
@property (nonatomic, strong) IBOutlet UIStackView*          weatherIconStackView;
@property (nonatomic, strong) IBOutlet UILabel*              tempLabel;
@property (nonatomic, strong) IBOutlet UILabel*              tempRangeLabel;

@end

@implementation MTStartViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self showNoWeatherMessage];
    [self.output updateView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tabBarController setTitle:@"Главная"];
    self.tabBarController.navigationItem.rightBarButtonItem = nil;
    self.tabBarController.navigationItem.rightBarButtonItems = nil;
}

- (void)showStartLocationView {
    _noLocationView.hidden = YES;
    _locationView.hidden = NO;
}

- (void)showNoStartLocationView {
    _noLocationView.hidden = NO;
    _locationView.hidden = YES;
}

- (void)showNoWeatherMessage {
    _currentWeatherView.hidden = YES;
    _noContentView.hidden = NO;
}

- (void)showCurrentWeather:(MTWeatherModel *)weatherModel {
    _currentWeatherView.hidden = NO;
    _noContentView.hidden = YES;
    _cityLabel.text = weatherModel.cityName;
    _tempLabel.text = [weatherModel temperatureString];
    _tempRangeLabel.text = [weatherModel temperatureRangeString];
    foreach(weatherItem, weatherModel.weatherItems) {
        let imageView = [[UIImageView alloc] init];
        imageView.backgroundColor = [UIColor clearColor];
        [imageView.heightAnchor constraintEqualToConstant:50].active = true;
        [imageView.widthAnchor constraintEqualToConstant:50].active = true;
        [_weatherIconStackView addArrangedSubview:imageView];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            let imgURL = [weatherItem urlForIcon];
            let data = [NSData dataWithContentsOfURL:imgURL];
            dispatch_async(dispatch_get_main_queue(), ^{
                [imageView setImage:[UIImage imageWithData:data]];
            });
        });
    }
    _weatherIconStackView.translatesAutoresizingMaskIntoConstraints = false;
}

- (void)showStartLocationViewWithCity:(NSString *)cityName address:(NSString *)address {
    [self showStartLocationView];
    _startTitle.text = cityName;
    _startSubtitle.text = address;
}

- (IBAction)startLocationSelect:(id)sender {
    [self.output addStartLocation];
}

@end
