//
//  MTStartModuleInterface.h
//  MTrip
//


#import <Foundation/Foundation.h>

@protocol MTStartViewOuput <NSObject>

- (void)updateView;
- (void)addStartLocation;

@end
