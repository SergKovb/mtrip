//
//  MTstartInteratorIO.h
//  MTrip
//


#import <Foundation/Foundation.h>

@class MTWeatherModel;
@class MTGeoObject;
@class CLLocation;

@protocol MTStartInteractorInput <NSObject>

- (void)loadGeoObjectForStartLocation;

@end

@protocol MTStartInteractorOutput <NSObject>

- (void)didLoadCurrentWeather:(MTWeatherModel *)weatherModel;
- (void)didLoadGeoObjectForStartLocation:(MTGeoObject *)geoObject;
- (void)didFailedLoadGeoObjectForStartLocation;

@end
