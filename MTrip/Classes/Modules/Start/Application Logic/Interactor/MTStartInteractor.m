//
//  MTStartInterator.m
//  MTrip
//

#define kTimeIntervalBetweenRequest 1200

#import "MTStartInteractor.h"
#import "MTStartDataManager.h"
#import "MTWeatherModel.h"
#import <CoreLocation/CoreLocation.h>
#import "MTWeatherLoaderService.h"
#import "MTGeocodeItem.h"
#import "MTStartLocationManager.h"
#import <ReactiveObjC.h>
#import "MTReverseGeocodeService.h"


@interface MTStartInteractor()

@property (nonatomic, strong)   MTStartLocationManager      *startLocationManager;
@property (nonatomic, strong)   MTStartDataManager          *dataManager;

@end


@implementation MTStartInteractor

- (instancetype)initWithDataManager:(MTStartDataManager *)dataManager
               startLocationManager:(MTStartLocationManager *)startLocationManager {
    if ((self = [super init])) {
        _dataManager = dataManager;
        _startLocationManager = startLocationManager;
    }
    return self;
}

- (void)setup {
    [self initScheduler];
    [self initStartLocationObserve];
}

- (void)initStartLocationObserve {
    __weak typeof(self) weakSelf = self;
    [RACObserve(_startLocationManager, startLocation)
     subscribeNext:^(id  _Nullable x) {
         __strong __typeof(weakSelf) strongSelf = weakSelf;
         [strongSelf loadWeatherForStartLocation];
         [strongSelf loadGeoObjectForStartLocation];
     }];
}

- (void)initScheduler {
    let timerSignal = [RACSignal interval:20*60
                              onScheduler:[RACScheduler mainThreadScheduler]];
    __weak typeof(self) weakSelf = self;
    [[RACSignal combineLatest:@[timerSignal]]
     subscribeNext:^(id x) {
         __strong __typeof(weakSelf) strongSelf = weakSelf;
         [strongSelf loadWeatherForStartLocation];
     }];
}

- (void)loadWeatherForStartLocation {
    __weak typeof(self) weakSelf = self;
    [self.weatherLoaderService loadCurrentWeatherForLocation:_startLocationManager.startLocation
                                             block:^(MTWeatherModel *weatherModel) {
                                                 __strong __typeof(weakSelf) strongSelf = weakSelf;
                                                 [strongSelf.output didLoadCurrentWeather:weatherModel];
                                             } failure:^(NSError *error) {
                                                 __strong __typeof(weakSelf) strongSelf = weakSelf;
                                                 [strongSelf.output didLoadCurrentWeather:nil];
                                             }];
}

- (void)loadGeoObjectForStartLocation {
    __weak typeof(self) weakSelf = self;
    [_reverseGeocodeService loadYandexReverseGeocodeWithLocation:_startLocationManager.startLocation
                                                     block:^(MTGeoObject *geoObject) {
                                                         __strong __typeof(weakSelf) strongSelf = weakSelf;
                                                         [strongSelf.output didLoadGeoObjectForStartLocation:geoObject];
                                                     }
                                                   failure:^{
                                                       __strong __typeof(weakSelf) strongSelf = weakSelf;
                                                       [strongSelf.output didFailedLoadGeoObjectForStartLocation];
                                                   }];
}



@end

