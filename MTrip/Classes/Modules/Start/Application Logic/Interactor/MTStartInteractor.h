//
//  MTStartInterator.h
//  MTrip
//

#import <Foundation/Foundation.h>

#import "MTStartInteractorIO.h"

@class MTStartDataManager;
@class MTStartLocationManager;
@protocol MTReverseGeocodeService;
@protocol MTWeatherLoaderService;

@interface MTStartInteractor : NSObject <MTStartInteractorInput>

@property (nonatomic, weak)     id <MTStartInteractorOutput> output;
@property (nonatomic,strong)    id <MTReverseGeocodeService> reverseGeocodeService;
@property (nonatomic,strong)    id <MTWeatherLoaderService> weatherLoaderService;

- (instancetype)initWithDataManager:(MTStartDataManager *)dataManager
               startLocationManager:(MTStartLocationManager *)startLocationManager;
- (void)setup;

@end
