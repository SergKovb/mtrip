//
//  MTRoutesDataManager.h
//  MTrip
//

#import <Foundation/Foundation.h>

@class MTRoutesDataStore;
@class MTRouteItem;

@interface MTRoutesDataManager : NSObject

@property (nonatomic, strong) MTRoutesDataStore *dataStore;

- (void)getSavedRoutesWithCompletionBlock:(void (^)(NSArray <MTRouteItem *>*routeItems))completionBlock;
- (void)deleteSavedRouteItem:(MTRouteItem *)routeItem
         withCompletionBlock:(void (^)(void))completionBlock;

@end
