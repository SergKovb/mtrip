//
//  MTRoutesDataManager.m
//  MTrip
//

#import "MTRoutesDataManager.h"
#import "NSArray+HOM.h"
#import "MTRoutesDataStore.h"
#import "MTManagedRouteItem.h"
#import "MTRouteItem.h"


@implementation MTRoutesDataManager

- (void)getSavedRoutesWithCompletionBlock:(void (^)(NSArray <MTRouteItem *> *routeItems))completionBlock{
    __weak typeof(self) weakSelf = self;
    [self.dataStore fetchRouteItemsWithCompletionBlock:^(NSArray<MTManagedRouteItem *> *results) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (completionBlock) {
            completionBlock([strongSelf routeItemsFromDataStoreEntries:results]);
        }
    }];
}

- (NSArray <MTRouteItem *> *)routeItemsFromDataStoreEntries:(NSArray <MTManagedRouteItem *> *)entries {
    return [entries arrayFromObjectsCollectedWithBlock:^id(MTManagedRouteItem *route) {
        let item =  [MTRouteItem routeItemWithName:route.name lat:route.lat lon:route.lon];
        [item setManagedRouteSteps:[route.routeSteps array]];
        item.distance = route.distance;
        return  item;
    }];
}

- (void)deleteSavedRouteItem:(MTRouteItem *)routeItem
         withCompletionBlock:(void (^)(void))completionBlock{
    __weak typeof(self) weakSelf = self;
    [self.dataStore deleteRouteWithLat:routeItem.lat
                                   lon:routeItem.lon
                               success:^{
                                   if (!weakSelf) {
                                       return;
                                   }
                                   __strong __typeof(weakSelf) strongSelf = weakSelf;
                                   [strongSelf.dataStore save];
                                   completionBlock();
                               }];
}


@end

