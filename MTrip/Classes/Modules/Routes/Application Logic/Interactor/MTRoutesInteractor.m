//
//  MTRoutesInterator.m
//  MTrip
//

#define APPID @"3294fbb1429b6c71a7769075be79bd42"
#define kTimeIntervalBetweenRequest 1200

#import "MTRoutesInteractor.h"
#import "MTRoutesDataManager.h"
#import "AFNetworking.h"
#import "MTRouteItem.h"

@interface MTRoutesInteractor()

@property (nonatomic, strong)   MTRoutesDataManager *dataManager;

@end


@implementation MTRoutesInteractor

- (instancetype)initWithDataManager:(MTRoutesDataManager *)dataManager {
    if ((self = [super init])) {
        _dataManager = dataManager;
    }
    
    return self;
}

- (void)findRoutes {
    __weak typeof(self) weakSelf = self;
    [self.dataManager getSavedRoutesWithCompletionBlock:^(NSArray<MTRouteItem *> *routeItems) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf.output foundRoutes:routeItems];
    }];
}

- (void)deleteRoute:(MTRouteItem *)routeItem {
    __weak typeof(self) weakSelf = self;
    [self.dataManager deleteSavedRouteItem:routeItem
                       withCompletionBlock:^{
                           if (!weakSelf) {
                               return;
                           }
                           __strong __typeof(weakSelf) strongSelf = weakSelf;
                           [strongSelf.output didDeleteRoute];
                       }];
}

@end
