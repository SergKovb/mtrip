//
//  MTRoutesInteratorIO.h
//  MTrip
//

#import <Foundation/Foundation.h>

@class MTRouteItem;

@protocol MTRoutesInteractorInput <NSObject>
- (void)findRoutes;
- (void)deleteRoute:(MTRouteItem *)routeItem;
@end


@protocol MTRoutesInteractorOutput <NSObject>
- (void)foundRoutes:(NSArray <MTRouteItem *> *)routes;
- (void)didDeleteRoute;
@end

