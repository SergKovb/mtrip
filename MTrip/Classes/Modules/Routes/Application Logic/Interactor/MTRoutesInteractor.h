//
//  MTRoutesInterator.h
//  MTrip
//

#import <Foundation/Foundation.h>
#import "MTRoutesInteractorIO.h"

@class MTRoutesDataManager;

@interface MTRoutesInteractor : NSObject <MTRoutesInteractorInput>

@property (nonatomic, weak)     id<MTRoutesInteractorOutput> output;

- (instancetype)initWithDataManager:(MTRoutesDataManager *)dataManager;


@end
