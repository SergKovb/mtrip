//
//  MTRoutesViewController.h
//  MTrip
//

#import <UIKit/UIKit.h>
#import "MTRoutesViewInput.h"
#import "MTRoutesViewOutput.h"

@class MTRoutesDataDisplayManager;

@interface MTRoutesViewController : UITableViewController <MTRoutesViewInput>

@property (nonatomic, strong) id<MTRoutesViewOutput>    output;
@property (nonatomic, strong) MTRoutesDataDisplayManager *dataDisplayManager;

@end
