//
//  MTRoutesModuleInterface.h
//  MTrip
//


#import <Foundation/Foundation.h>

@class MTRouteItem;

@protocol MTRoutesViewOutput <NSObject>

- (void)updateView;
- (void)addRouteButtonClicked;

@end
