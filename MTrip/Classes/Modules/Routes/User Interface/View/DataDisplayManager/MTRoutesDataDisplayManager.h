//
//  MTRoutesDataDisplayManager.h
//  MTrip
//

#import <Foundation/Foundation.h>
#import "DataDisplayManager.h"

NS_ASSUME_NONNULL_BEGIN

@class MTRouteItem;

@protocol MTRoutesDataDisplayManagerDelegate <NSObject>

- (void)didTapRouteCellWithRouteItem:(MTRouteItem *)routeItem;
- (void)didDeleteRouteCellWithRouteItem:(MTRouteItem *)routeItem;

@end


@interface MTRoutesDataDisplayManager : NSObject <DataDisplayManager>

@property (nonatomic, weak) id <MTRoutesDataDisplayManagerDelegate> delegate;

- (void)configureDataDisplayManagerWithRouteItems:(NSArray <MTRouteItem *> *)routeItems;

@end

NS_ASSUME_NONNULL_END
