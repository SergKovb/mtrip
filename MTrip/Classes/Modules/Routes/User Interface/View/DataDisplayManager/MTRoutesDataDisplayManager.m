//
//  MTRoutesDataDisplayManager.m
//  MTrip
//

#import "MTRoutesDataDisplayManager.h"
#import "MTRouteTableViewCell.h"
#import "MTRouteItem.h"

static let MTRouteCellIdentifier = @"MTRouteCell";


@interface MTRoutesDataDisplayManager () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray <MTRouteItem *> *routeItems;

@end

@implementation MTRoutesDataDisplayManager

- (void)configureDataDisplayManagerWithRouteItems:(NSArray<MTRouteItem *> *)routeItems {
    self.routeItems = routeItems;
}

#pragma mark DataDisplayManager

- (id<UITableViewDataSource>)dataSourceForTableView:(UITableView *)tableView {
    return self;
}

- (id<UITableViewDelegate>)delegateForTableView:(UITableView *)tableView {
    return self;
}

#pragma mark UITableViewDataSource

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    let routeItem = self.routeItems[indexPath.row];
    let cell = (MTRouteTableViewCell *)[tableView dequeueReusableCellWithIdentifier:MTRouteCellIdentifier forIndexPath:indexPath];
    cell.name.text = routeItem.name;
    cell.distance.text =  [NSString stringWithFormat:@"%.0f км",routeItem.distance/1000];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.routeItems.count;
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    let routeItem = self.routeItems[indexPath.row];
    [self.delegate didTapRouteCellWithRouteItem:routeItem];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        let routeItem = self.routeItems[indexPath.row];
        [self.delegate didDeleteRouteCellWithRouteItem:routeItem];
    }
}

@end
