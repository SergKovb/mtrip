//
//  MTRoutesViewController.m
//  MTrip
//

#import "MTRoutesViewController.h"
#import "MTWeatherModel.h"
#import "MTRouteDisplayData.h"
#import "MTRouteTableViewCell.h"
#import "MTRouteItem.h"
#import "MTRoutesDataDisplayManager.h"



@interface MTRoutesViewController ()

@property (nonatomic, strong) IBOutlet UIView*              noContentView;
@property (nonatomic, strong) IBOutlet UIView*              contentView;
@property (nonatomic, strong) IBOutlet UITableView*         strongTableView;

@end

@implementation MTRoutesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self configureView];
    [self.output updateView];
}

- (void)configureView {
    let addItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                target:self
                                                                action:@selector(didTapAddButton:)];
    addItem.tintColor = [UIColor whiteColor];
    [self.tabBarController setTitle:@"Маршруты"];
    self.tabBarController.navigationItem.rightBarButtonItem = addItem;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.strongTableView.dataSource = [self.dataDisplayManager dataSourceForTableView:self.tableView];
    self.strongTableView.delegate = [self.dataDisplayManager delegateForTableView:self.tableView];
}

- (void)didTapAddButton:(id)sender {
    [self.output addRouteButtonClicked];
}

- (void)showNoContentMessage {
    self.view = self.noContentView;
}

- (void)showRouteDisplayData:(MTRouteDisplayData *)data {
    self.view = self.contentView;
    [self.dataDisplayManager configureDataDisplayManagerWithRouteItems:data.routes];
    [_strongTableView reloadData];
}

- (void)reloadEntries {
    [_strongTableView reloadData];
}

@end

