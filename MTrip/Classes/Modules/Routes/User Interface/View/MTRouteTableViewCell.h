//
//  MTRouteTableViewCell.h
//  MTrip
//

#import <UIKit/UIKit.h>

@interface MTRouteTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *name;
@property (nonatomic, weak) IBOutlet UILabel *distance;
@end
