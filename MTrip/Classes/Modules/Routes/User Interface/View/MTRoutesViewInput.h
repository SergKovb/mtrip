//
//  MTRoutesViewInterface.h
//  MTrip
//


#import <Foundation/Foundation.h>

@class  MTRouteDisplayData;

@protocol MTRoutesViewInput <NSObject>

- (void)showNoContentMessage;
- (void)showRouteDisplayData:(MTRouteDisplayData *)data;
- (void)reloadEntries;

@end
