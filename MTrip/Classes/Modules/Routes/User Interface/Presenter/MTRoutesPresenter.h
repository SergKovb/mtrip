//
//  MTRoutesPresenter.h
//  MTrip
//

#import <Foundation/Foundation.h>
#import "MTRoutesViewOutput.h"
#import "MTRoutesInteractorIO.h"
#import "MTRoutesRouter.h"
#import "MTRoutesViewInput.h"
#import "MTRoutesDataDisplayManager.h"

@protocol MTRoutesViewInput;

@interface MTRoutesPresenter : NSObject <MTRoutesInteractorOutput, MTRoutesViewOutput, MTRoutesDataDisplayManagerDelegate>

@property (nonatomic, strong) id<MTRoutesInteractorInput>     routesInteractor;
@property (nonatomic, strong) MTRoutesRouter*              routesRouter;
@property (nonatomic, weak) id<MTRoutesViewInput> userInterface;

@end
