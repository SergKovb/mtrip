//
//  MTRoutesPresenter.m
//  MTrip
//

#import "MTRoutesPresenter.h"
#import "MTRouteDisplayData.h"

@implementation MTRoutesPresenter

#pragma mark MTRoutesViewOutput

- (void)updateView {
    [self.routesInteractor findRoutes];
}

- (void)addRouteButtonClicked {
    [self.routesRouter presentAddPointInterface];
}

#pragma mark MTRoutesDataDisplayManagerDelegate

- (void)didDeleteRouteCellWithRouteItem:(MTRouteItem *)routeItem {
    [self.routesInteractor deleteRoute:routeItem];
}

- (void)didTapRouteCellWithRouteItem:(nonnull MTRouteItem *)routeItem {
    [self.routesRouter presentRouteDetailInterfaceForRouteItem:routeItem];
}

#pragma mark MTRoutesInteractorOutput

- (void)foundRoutes:(NSArray<MTRouteItem *> *)routes {
    if ([routes count] == 0) {
        [self.userInterface showNoContentMessage];
    }
    else {
        [self updateUserInterfaceWithUpcomingItems:routes];
    }
}

- (void)didDeleteRoute {
    [self updateView];
}

- (void)updateUserInterfaceWithUpcomingItems:(NSArray<MTRouteItem *> *)upcomingItems {
    [self.userInterface showRouteDisplayData:[self upcomingDisplayDataWithItems:upcomingItems]];
}

- (MTRouteDisplayData *)upcomingDisplayDataWithItems:(NSArray<MTRouteItem *> *)upcomingItems {
    let data = [MTRouteDisplayData upcomingDisplayDataWithRoutes:upcomingItems];
    return data;
}



@end
