//
//  MTRouteDisplayData.h
//  MTrip
//

#import <Foundation/Foundation.h>

@class MTRouteItem;

@interface MTRouteDisplayData : NSObject

@property (nonatomic, readonly, copy,) NSArray <MTRouteItem *>*    routes;

+ (instancetype)upcomingDisplayDataWithRoutes:(NSArray <MTRouteItem *> *)routes;
@end
