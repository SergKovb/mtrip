//
//  MTRouteDisplayData.m
//  MTrip
//

#import "MTRouteDisplayData.h"
#import "MTRouteItem.h"


@interface MTRouteDisplayData()
@property (nonatomic, copy,) NSArray <MTRouteItem *>*    routes;
@end

@implementation MTRouteDisplayData

+ (instancetype)upcomingDisplayDataWithRoutes:(NSArray<MTRouteItem *> *)routes {
    let data = [[MTRouteDisplayData alloc] init];
    
    data.routes = routes;
    
    return data;
}

- (BOOL)isEqualToUpcomingDisplayData:(MTRouteDisplayData *)data {
    if (!data) {
        return NO;
    }
    BOOL hasEqualRoutes = [self.routes isEqualToArray:data.routes];
    return hasEqualRoutes;
}


- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    
    if (![object isKindOfClass:[MTRouteDisplayData class]]) {
        return NO;
    }
    
    return [self isEqualToUpcomingDisplayData:object];
}


- (NSUInteger)hash {
    return [self.routes hash];
}


@end
