//
//  MTRoutesRouter.m
//  MTrip
//

#import "MTRoutesRouter.h"
#import "MTRoutesPresenter.h"
#import "MTRoutesViewController.h"
#import "MTAddPointRouter.h"
#import "MTRouteDetailRouter.h"
#import "MTAddPointInput.h"
#import "MTRouteDetailInput.h"

static NSString* const MTAddPointSegue = @"addPoint";
static NSString* const MTRouteDetailSegue = @"routeDetail";

@implementation MTRoutesRouter

- (void)presentAddPointInterface {
    [[self.transitionHandler openModuleUsingSegue:MTAddPointSegue]
     thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<MTAddPointInput> moduleInput) {
         [moduleInput configureForAddPoint];
         return nil;
     }];
}

- (void)presentRouteDetailInterfaceForRouteItem:(MTRouteItem *)routeItem {
    [[self.transitionHandler openModuleUsingSegue:MTRouteDetailSegue]
     thenChainUsingBlock:^id<RamblerViperModuleOutput>(id<MTRouteDetailInput> moduleInput) {
         [moduleInput configureForRouteItem:routeItem];
         return nil;
     }];
}

- (void)updateRoutesView {
    //to-do
}


@end
