//
//  MTRoutesRouterInput.h
//  MTrip
//

@class MTRouteItem;
@protocol MTRoutesRouterInput <NSObject>

- (void)presentAddPointInterface;
- (void)presentRouteDetailInterfaceForRouteItem:(MTRouteItem *)routeItem;

@end
