//
//  MTRoutesRouter.h
//  MTrip
//

#import <Foundation/Foundation.h>
#import "MTRoutesRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;
@protocol RamblerViperModuleFactoryProtocol;

@interface MTRoutesRouter : NSObject <MTRoutesRouterInput>

@property (nonatomic,weak) id<RamblerViperModuleTransitionHandlerProtocol>  transitionHandler;
@property (nonatomic,strong) id<RamblerViperModuleFactoryProtocol>          routeDetailModuleFactory;
@property (nonatomic,strong) id<RamblerViperModuleFactoryProtocol>          addPointModuleFactory;

- (void)updateRoutesView;

@end
