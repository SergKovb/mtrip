//
//  MTRoutesModuleAssembly.m
//  MTrip
//

#import "MTRoutesModuleAssembly.h"

#import "MTRouteDetailModuleAssembly.h"

#import "MTRoutesViewController.h"
#import "MTRoutesInteractor.h"
#import "MTRoutesDataManager.h"
#import "MTRoutesRouter.h"
#import "MTRoutesPresenter.h"
#import "MTStartLocationManager.h"
#import "MTRoutesDataStore.h"
#import "MTRoutesDataDisplayManager.h"



@interface  MTRoutesModuleAssembly()

@property (nonatomic,strong,readonly) MTRouteDetailModuleAssembly       *routeDetailModuleAssembly;


@end

@implementation MTRoutesModuleAssembly


- (MTRoutesPresenter *)presenterModuleRoutes {
    return [TyphoonDefinition withClass:[MTRoutesPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(routesInteractor)
                                                    with:[self interactorModuleRoutes]];
                              [definition injectProperty:@selector(routesRouter)
                                                    with:[self routerModuleRoutes]];
                              [definition injectProperty:@selector(userInterface)
                                                    with:[self viewModuleRoutes]];
                          }];
}

- (MTRoutesInteractor *)interactorModuleRoutes {
    return [TyphoonDefinition withClass:[MTRoutesInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition useInitializer:@selector(initWithDataManager:)
                                              parameters:^(TyphoonMethod *initializer) {
                                                  [initializer injectParameterWith:[self dataManagerModuleRoutes]];
                                              }];
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterModuleRoutes]];
                          }];
}

- (MTRoutesRouter *)routerModuleRoutes {
    return [TyphoonDefinition withClass:[MTRoutesRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewModuleRoutes]];
                          }];
}

- (MTRoutesDataManager *)dataManagerModuleRoutes {
    return [TyphoonDefinition withClass:[MTRoutesDataManager class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(dataStore)
                                                    with:[MTRoutesDataStore new]];
                          }];
}


- (MTRoutesViewController *)viewModuleRoutes {
    return [TyphoonDefinition withClass:[MTRoutesViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterModuleRoutes]];
                              [definition injectProperty:@selector(dataDisplayManager)
                                                    with:[self dataDisplayManagerModuleRoutes]];
                          }];
}

- (MTRoutesDataDisplayManager *)dataDisplayManagerModuleRoutes {
    return [TyphoonDefinition withClass:[MTRoutesDataDisplayManager class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(delegate)
                                                    with:[self presenterModuleRoutes]];
                          }];
}
@end
