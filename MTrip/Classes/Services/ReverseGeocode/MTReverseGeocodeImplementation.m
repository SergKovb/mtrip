//
//  MTReverseGeocode.m
//  MTrip
//

#import "MTReverseGeocodeImplementation.h"
#import <CoreLocation/CoreLocation.h>
#import <AFNetworking.h>
#import "MTGeocodeItem.h"
#import "Constants.h"
#import "MTReverseGeocodeService.h"


@implementation MTReverseGeocodeImplementation


- (void)loadAppleReverseGeocodeWithLocation:(CLLocation *)location
                                      block:(void (^)(CLPlacemark *))success
                                    failure:(void (^)(void))failure {
    let geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
                       if(placemarks.count){
                           success(placemarks.firstObject);
                       } else {
                           failure();
                       }
                   }];
}


- (void)loadYandexReverseGeocodeWithLocation:(CLLocation *)location
                                       block:(void (^)(MTGeoObject *geoObject))success
                                     failure:(void (^)(void))failure {
    if (!location) {
        failure();
        return;
    }
    var currentGeocodeUrlString = [NSString stringWithFormat:YandexReverseGeocodeURL,location.coordinate.longitude,location.coordinate.latitude];
    currentGeocodeUrlString = [currentGeocodeUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    let configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    let manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    let URL = [NSURL URLWithString:currentGeocodeUrlString];
    let request = [NSURLRequest requestWithURL:URL];
    
    let dataTask = [manager dataTaskWithRequest:request
                                 uploadProgress:nil
                               downloadProgress:nil
                              completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                  [manager invalidateSessionCancelingTasks:YES];
                                  if (error) {
                                      failure();
                                  } else {
                                      success([self getLocationGeoObjectFromResponseData:responseObject]);
                                  }
                              }];
    [dataTask resume];
}

#pragma mark Private methods

- (MTGeoObject *)getLocationGeoObjectFromResponseData:(NSData *)data {
    let responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSError *error;
    let geocodeItem = [[MTGeocodeItem alloc] initWithString:responseString error:&error];
    return geocodeItem.featureMember.firstObject;
}

@end
