//
//  MTReverseGeocode.h
//  MTrip
//

@class MTGeoObject;
@class CLPlacemark;
@class CLLocation;

@protocol MTReverseGeocodeService

- (void)loadYandexReverseGeocodeWithLocation:(CLLocation *)location
                                       block:(void (^)(MTGeoObject *geoObject))success
                                     failure:(void (^)(void))failure;

- (void)loadAppleReverseGeocodeWithLocation:(CLLocation *)location
                                      block:(void (^)(CLPlacemark *))success
                                    failure:(void (^)(void))failure;

@end
