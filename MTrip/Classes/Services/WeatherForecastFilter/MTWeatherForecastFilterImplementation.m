//
//  MTWeatherForecastFilterImplementation.m
//  MTrip
//


#import "MTWeatherForecastFilterImplementation.h"
#import "MTWeatherForecastModel.h"

@implementation MTWeatherForecastFilterImplementation

- (MTWeatherModel *)weatherModelFromForecast:(MTWeatherForecastModel *)weatherForecastModel
                             forTimeInterval:(NSInteger)timeInterval
                                   sinceDate:(NSDate *)date {
    let timeIntervalSince1970 = [date timeIntervalSince1970];
    let resultInterval = timeIntervalSince1970 + timeInterval;
  
    let mutableArray = (NSMutableArray <MTWeatherModel *> *)[NSMutableArray arrayWithArray:weatherForecastModel.weatherModels];
    
    [mutableArray sortUsingComparator:^NSComparisonResult(MTWeatherModel   * _Nonnull obj1, MTWeatherModel  * _Nonnull obj2) {
        int num1 = fabs(obj1.dt - resultInterval);
        int num2 = fabs(obj2.dt - resultInterval) ;
        return (num1 < num2) ? NSOrderedAscending :
        (num1 > num2) ? NSOrderedDescending :
        NSOrderedSame;
    }];
    return mutableArray.firstObject;
}

- (MTWeatherModel *)weatherModelFromForecast:(MTWeatherForecastModel *)weatherForecastModel
                             forTimeInterval:(NSInteger)timeInterval {
    return [self weatherModelFromForecast:weatherForecastModel
                          forTimeInterval:timeInterval
                                sinceDate:[NSDate new]];
}



@end
