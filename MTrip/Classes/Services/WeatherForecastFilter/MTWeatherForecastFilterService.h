//
//  MTWeatherForecastFilterService.h
//  MTrip
//

#import <Foundation/Foundation.h>

@class MTWeatherForecastModel;
@class MTWeatherModel;

@protocol MTWeatherForecastFilterService

- (MTWeatherModel *)weatherModelFromForecast:(MTWeatherForecastModel *)weatherForecastModel
                             forTimeInterval:(NSInteger)timeInterval
                                   sinceDate:(NSDate *)date;
- (MTWeatherModel *)weatherModelFromForecast:(MTWeatherForecastModel *)weatherForecastModel
                             forTimeInterval:(NSInteger)timeInterval;

@end

