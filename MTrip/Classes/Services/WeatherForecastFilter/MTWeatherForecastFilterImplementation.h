//
//  MTWeatherForecastFilterImplementation.h
//  MTrip
//

#import <Foundation/Foundation.h>
#import "MTWeatherForecastFilterService.h"

NS_ASSUME_NONNULL_BEGIN

@interface MTWeatherForecastFilterImplementation : NSObject <MTWeatherForecastFilterService>

@end

NS_ASSUME_NONNULL_END
