//
//  MTDirectionsFinderService.h
//  MTrip
//

@class MKRoute;
@class CLLocation;

typedef void (^MTRouteFinderServiceErrorBlock)(NSError *error);

@protocol MTRouteFinderService

- (void)routeFrom:(CLLocation *)location
               to:(CLLocation *)destination
          success:(void (^)(MKRoute *route))success
          failure:(MTRouteFinderServiceErrorBlock)failure;

- (void)routeFrom:(CLLocation *)location
               to:(CLLocation *)destination
          success:(void (^)(MKRoute *route))success;

@end
