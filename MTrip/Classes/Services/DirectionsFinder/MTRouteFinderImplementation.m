//
//  MTDirectionsFinder.m
//  MTrip
//

#import <MapKit/MapKit.h>
#import "MTRouteFinderImplementation.h"

@implementation MTRouteFinderImplementation

- (void)routeFrom:(CLLocation *)location
               to:(CLLocation *)destination
          success:(void (^)(MKRoute *))success
          failure:(MTRouteFinderServiceErrorBlock)failure {
    let locationPlacemark = [[MKPlacemark alloc] initWithCoordinate:location.coordinate addressDictionary:nil];
    let destinationPlacemark = [[MKPlacemark alloc] initWithCoordinate:destination.coordinate addressDictionary:nil];
    
    let directionsRequest = [[MKDirectionsRequest alloc] init];
    [directionsRequest setSource:[[MKMapItem alloc] initWithPlacemark:locationPlacemark]];
    [directionsRequest setDestination:[[MKMapItem alloc] initWithPlacemark:destinationPlacemark]];
    [directionsRequest setTransportType:MKDirectionsTransportTypeAutomobile];
    
    let directions = [[MKDirections alloc] initWithRequest:directionsRequest];
    [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        if (error) {
            failure(error);
        } else {
            success (response.routes.firstObject);
        }
    }];
}

- (void)routeFrom:(CLLocation *)location
               to:(CLLocation *)destination
          success:(void (^)(MKRoute *))success {
    [self routeFrom:location
                 to:destination
            success:success
            failure:nil];
}

@end
