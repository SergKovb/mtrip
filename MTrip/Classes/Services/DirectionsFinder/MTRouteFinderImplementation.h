//
//  MTDirectionsFinder.h
//  MTrip
//


#import <Foundation/Foundation.h>
#import "MTRouteFinderService.h"

@interface MTRouteFinderImplementation : NSObject <MTRouteFinderService>

@end
