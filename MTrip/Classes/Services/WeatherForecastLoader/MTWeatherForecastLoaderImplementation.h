//
//  MTWeatherForecastLoaderImplementation.h
//  MTrip
//

#import <Foundation/Foundation.h>
#import "MTWeatherForecastLoaderService.h"

NS_ASSUME_NONNULL_BEGIN

@interface MTWeatherForecastLoaderImplementation : NSObject <MTWeatherForecastLoaderService>

@end

NS_ASSUME_NONNULL_END
