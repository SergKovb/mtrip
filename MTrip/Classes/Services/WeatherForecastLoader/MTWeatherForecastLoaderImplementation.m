//
//  MTWeatherForecastLoaderImplementation.m
//  MTrip
//

#import "MTWeatherForecastLoaderImplementation.h"
#import <CoreLocation/CoreLocation.h>
#import "Constants.h"
#import "AFNetworking.h"
#import "MTWeatherForecastModel.h"

@implementation MTWeatherForecastLoaderImplementation

- (void)loadWeatherForLocation:(CLLocation *)location
                         block:(void (^)(MTWeatherForecastModel *))success
                       failure:(MTWeatherLoaderServiceErrorBlock)failure {
    var currentWeatherUrlString = [NSString stringWithFormat:@"%s?lat=%f&lon=%f&appid=%@&units=metric",WeatherForeCastUrl,location.coordinate.latitude,location.coordinate.longitude,APPID];
    currentWeatherUrlString = [currentWeatherUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    let configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    let manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    let URL = [NSURL URLWithString:currentWeatherUrlString];
    let request = [NSURLRequest requestWithURL:URL];
    
    let dataTask = [manager dataTaskWithRequest:request
                                 uploadProgress:nil
                               downloadProgress:nil
                              completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                  [manager invalidateSessionCancelingTasks:YES];
                                  if (error) {
                                      failure(error);
                                  } else {
                                      let responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                                      NSError *error;
                                      let weatherForecastModel = [[MTWeatherForecastModel alloc] initWithString:responseString error:&error];
                                      success(weatherForecastModel);
                                  }
                              }];
    [dataTask resume];
}

- (void)loadWeatherForLocation:(CLLocation *)location
                         block:(void (^)(MTWeatherForecastModel *))success {
    [self loadWeatherForLocation:location
                           block:success
                         failure:nil];
}

@end
