//
//  MTWeatherForecastLoaderService.h
//  MTrip
//

#import <Foundation/Foundation.h>

@class MTWeatherForecastModel;
@class CLLocation;

typedef void (^MTWeatherLoaderServiceErrorBlock)(NSError *error);

@protocol MTWeatherForecastLoaderService

- (void)loadWeatherForLocation: (CLLocation *)location
                         block:(void (^)(MTWeatherForecastModel *weatherModel))success
                       failure:(MTWeatherLoaderServiceErrorBlock)failure;
- (void)loadWeatherForLocation: (CLLocation *)location
                         block:(void (^)(MTWeatherForecastModel *weatherModel))success;

@end
