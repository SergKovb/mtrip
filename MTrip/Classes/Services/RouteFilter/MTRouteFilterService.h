//
//  MTRouteFilterService.h
//  MTrip
//

@class MKRouteStep;

@protocol MTRouteFilterService

- (NSArray <MKRouteStep *>*)filterSteps:(NSArray <MKRouteStep *> *)stepsArray
                             byDistance:(NSInteger)distance;

@end
