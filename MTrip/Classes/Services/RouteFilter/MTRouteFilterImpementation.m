//
//  MTRouteFilterServiceImpementation.m
//  MTrip
//

#import "MTRouteFilterImpementation.h"
#import <MapKit/MKDirectionsResponse.h>


@implementation MTRouteFilterImpementation

- (NSArray <MKRouteStep *>*)filterSteps:(NSArray <MKRouteStep *> *)stepsArray
              byDistance:(NSInteger)distance {
    __block var sum = 0.0;
    let newStepsArray = (NSMutableArray <MKRouteStep *> *)[NSMutableArray new];
    
    foreach(routeStep, stepsArray) {
        sum += routeStep.distance;
        if (sum > distance) {
            [newStepsArray addObject:routeStep];
            sum = 0;
        }
    }
    
    return [newStepsArray copy];
}

@end
