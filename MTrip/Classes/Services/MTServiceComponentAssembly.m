//
//  MTServiceComponentAssembly.m
//  MTrip
//

#import "MTServiceComponentAssembly.h"
#import "MTReverseGeocodeImplementation.h"
#import "MTRouteFinderImplementation.h"
#import "MTWeatherLoaderImplementation.h"
#import "MTWeatherForecastLoaderImplementation.h"
#import "MTRouteFilterImpementation.h"
#import "MTTimeFormatterImplementation.h"
#import "MTWeatherForecastFilterImplementation.h"

@implementation MTServiceComponentAssembly

- (id<MTReverseGeocodeService>)reverseGeocodeService {
    return [TyphoonDefinition withClass:[MTReverseGeocodeImplementation class]];
}

- (id<MTRouteFinderService>)routeFinderService {
    return [TyphoonDefinition withClass:[MTRouteFinderImplementation class]];
}

- (id<MTWeatherLoaderService>)weatherLoaderService {
    return [TyphoonDefinition withClass:[MTWeatherLoaderImplementation class]];
}

- (id<MTWeatherForecastLoaderService>)weatherForecastLoaderService {
    return [TyphoonDefinition withClass:[MTWeatherForecastLoaderImplementation class]];
}

- (id<MTRouteFilterService>)routeFilterService {
    return [TyphoonDefinition withClass:[MTRouteFilterImpementation class]];
}

- (id<MTTimeFormatterService>)timeFormatterService {
    return [TyphoonDefinition withClass:[MTTimeFormatterImplementation class]];
}

- (id<MTWeatherForecastFilterService>)weatherForecastFilterService {
    return [TyphoonDefinition withClass:[MTWeatherForecastFilterImplementation class]];
}

@end
