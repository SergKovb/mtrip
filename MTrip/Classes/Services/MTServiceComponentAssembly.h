//
//  MTServiceComponentAssembly.h
//  MTrip
//

#import "TyphoonAssembly.h"

NS_ASSUME_NONNULL_BEGIN

@protocol MTReverseGeocodeService;
@protocol MTRouteFinderService;
@protocol MTWeatherLoaderService;
@protocol MTWeatherForecastLoaderService;
@protocol MTRouteFilterService;
@protocol MTTimeFormatterService;
@protocol MTWeatherForecastFilterService;

@interface MTServiceComponentAssembly : TyphoonAssembly

- (id<MTReverseGeocodeService>)reverseGeocodeService;
- (id<MTRouteFinderService>)routeFinderService;
- (id<MTWeatherLoaderService>)weatherLoaderService;
- (id<MTWeatherForecastLoaderService>)weatherForecastLoaderService;
- (id<MTRouteFilterService>)routeFilterService;
- (id<MTTimeFormatterService>)timeFormatterService;
- (id<MTWeatherForecastFilterService>)weatherForecastFilterService;

@end

NS_ASSUME_NONNULL_END
