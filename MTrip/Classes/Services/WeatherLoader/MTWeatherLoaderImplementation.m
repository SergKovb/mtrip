//
//  MTWeatherLoader.m
//  MTrip
//

#import "MTWeatherLoaderImplementation.h"
#import "MTWeatherModel.h"
#import <CoreLocation/CoreLocation.h>
#import "AFNetworking.h"
#import "Constants.h"


@implementation MTWeatherLoaderImplementation

- (void)loadCurrentWeatherForLocation:(CLLocation *)location
                                block:(void (^)(MTWeatherModel *))success
                              failure:(MTWeatherLoaderServiceErrorBlock)failure {
    var currentWeatherUrlString = [NSString stringWithFormat:@"%s?lat=%f&lon=%f&appid=%@&units=metric",WeatherUrl,location.coordinate.latitude,location.coordinate.longitude,APPID];
    currentWeatherUrlString = [currentWeatherUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    let configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    let manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    let URL = [NSURL URLWithString:currentWeatherUrlString];
    let request = [NSURLRequest requestWithURL:URL];
    
    let dataTask = [manager dataTaskWithRequest:request
                                 uploadProgress:nil
                               downloadProgress:nil
                              completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                  [manager invalidateSessionCancelingTasks:YES];
                                  if (error) {
                                      failure(error);
                                  } else {
                                      let responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                                      NSError *error;
                                      let weatherModel = [[MTWeatherModel alloc] initWithString:responseString error:&error];
                                      success(weatherModel);
                                  }
                              }];
    [dataTask resume];
}

- (void)loadCurrentWeatherForLocation:(CLLocation *)location
                                block:(void (^)(MTWeatherModel *))success {
    [self loadCurrentWeatherForLocation:location
                                             block:success
                                           failure:nil];
}

@end

