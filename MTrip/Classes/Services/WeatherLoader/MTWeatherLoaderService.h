//
//  MTWeatherLoaderService.h
//  MTrip
//

@class MTWeatherModel;
@class CLLocation;

typedef void (^MTWeatherLoaderServiceErrorBlock)(NSError *error);

@protocol MTWeatherLoaderService

- (void)loadCurrentWeatherForLocation: (CLLocation *)location
                                block:(void (^)(MTWeatherModel *weatherModel))success
                              failure:(MTWeatherLoaderServiceErrorBlock)failure;
- (void)loadCurrentWeatherForLocation: (CLLocation *)location
                                block:(void (^)(MTWeatherModel *weatherModel))success;

@end
