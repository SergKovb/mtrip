//
//  MTTimeFormatterService.h
//  MTrip
//

#import <Foundation/Foundation.h>

@protocol MTTimeFormatterService

- (NSString *)stringFromTravelTimeInterval:(NSInteger)timeInterval;

@end
