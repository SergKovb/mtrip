//
//  MTTravelTimeFormatter.m
//  MTrip
//


#import "MTTimeFormatterImplementation.h"

@implementation MTTimeFormatterImplementation

- (NSString *)stringFromTravelTimeInterval:(NSInteger)timeInterval {
    let sysCalendar = [NSCalendar currentCalendar];
    
    // Create the NSDates
    let date1 = [NSDate new];
    let date2 = [[NSDate alloc] initWithTimeIntervalSinceNow:timeInterval];
    
    // Get conversion to months, days, hours, minutes
    let unitFlags =  NSCalendarUnitMinute | NSCalendarUnitHour;
    
    let breakdownInfo = [sysCalendar components:unitFlags fromDate:date1  toDate:date2  options:0];
    return [NSString stringWithFormat:@" %li ч: %li мин", (long)[breakdownInfo hour], (long)[breakdownInfo minute]];
}

@end
