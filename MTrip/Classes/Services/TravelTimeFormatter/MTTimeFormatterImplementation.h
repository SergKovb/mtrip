//
//  MTTravelTimeFormatter.h
//  MTrip
//

#import <Foundation/Foundation.h>
#import "MTTimeFormatterService.h"

NS_ASSUME_NONNULL_BEGIN

@interface MTTimeFormatterImplementation : NSObject <MTTimeFormatterService>


@end

NS_ASSUME_NONNULL_END
